import React ,{useEffect,useState}from 'react';
import { useHistory } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PointOverview from './pointsoverview';
import PromotionOverview from './promotionoverview'
import Typography from '@material-ui/core/Typography';
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { useQuery, gql } from '@apollo/client';

import './dashboard.css';


interface MemberInventory {
    id: string;
    promoType: string;
}

interface MemberInventoryData {
    memberInventory: MemberInventory[];
}

interface MemberInventoryVars {
    year: number;
}

  export const GET_MEMBER_QUERY = gql`
    query GetUserData {
        getUser{
            id
            givenName
            username
            email
            phoneNumber
            isActive
        }
    }
`;

export default function MemberOverview() {
    const history = useHistory();
    const mangeMemberView = () => history.push('/managemembers');
    const [memberCount, setMemberCount] = useState<any>("0");
    const {data, loading, refetch} = useQuery<any>(GET_MEMBER_QUERY);
    
    useEffect(() => {
        if(!loading){
            setMemberCount(data.getUser.length);
        }
    },[data]);


    return (
        <div  className="org-member-overview-report" >
            <Paper> 
           <Grid container sm={12} spacing={2}>
               <Grid item sm={4} xs={4}>
                    <svg viewBox="0 0 36 36" className="circular-chart orange">
                        <path className="circle-bg"
                            d="M18 2.0845
                            a 15.9155 15.9155 0 0 1 0 31.831
                            a 15.9155 15.9155 0 0 1 0 -31.831"
                        />
                        <path className="circle"
                            stroke-dasharray="100, 0"
                            d="M18 2.0845
                            a 15.9155 15.9155 0 0 1 0 31.831
                            a 15.9155 15.9155 0 0 1 0 -31.831"
                        />
                    </svg>
               </Grid>
               <Grid container sm={8} className="overview-chart-panel">
                   <label className="overview-title">MEMBER OVERVIEW</label>
                   <Grid item className="" xs={6}>
                        <span className="data-bold green">{memberCount}</span><label className="sub-caption nowrap">Total Member</label>
                   </Grid>
                   
               </Grid>
           </Grid>
           <Grid container sm={12}>
               <Grid className="btn-section" item  xs={12}>
                        <Button onClick={mangeMemberView} className="btn" variant="contained">Manage</Button>

               </Grid>
           </Grid>
           </Paper>
        </div>
    )
}
