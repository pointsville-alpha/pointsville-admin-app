import React, {useEffect,useState} from 'react';
import { useHistory } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import PointOverview from './pointsoverview';
import PromotionOverview from './promotionoverview'
import Typography from '@material-ui/core/Typography';
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { useQuery, gql } from '@apollo/client';

import './dashboard.css';


interface ClubInventory {
    id: number;
    name: string;
  }
  
  interface ClubInventoryData {
    clubInventory: ClubInventory[];
  }
  
  interface ClubInventoryVars {
    year: number;
  }
  
  
  export const GET_ORGS_QUERY = gql`
  query GetClubInventory {
    orgsAll{
    id
    name
    rate
    orgCode
    logoUrl
    availablePoints
    totalPoints
    distributionPoints
    isActive
  }
  }
  `;
export default function OrgOverview() {
    const history = useHistory();
    const mangeOrgOverView = () => history.push('/manageclubs');
    const [activeCount,setActiveCount] = useState<any>("0");
    const [inActiveCount,setInActiveCount] = useState<any>("0");
    const [chartValue,setChartValue] = useState<any>("50,50");
    const { refetch,loading, data } = useQuery<any>(
        GET_ORGS_QUERY
      );
    useEffect(() => {
        if(!loading){
            // const tamil = 100;
            var activeClub =  data.orgsAll.filter(function(org :any) { return org.isActive == true;});
            setActiveCount(activeClub.length);
            var inActiveClub =  data.orgsAll.filter(function(org :any) { return org.isActive == false;});
            setInActiveCount(inActiveClub.length);
            const active = activeClub.length * 100 / data.orgsAll.length;
            const inactive = inActiveClub.length * 100 / data.orgsAll.length;
            console.log(JSON.stringify(active+","+inactive));
            setChartValue(JSON.stringify(active+","+inactive));
        }
    },[data]);


    // var marvelHeroes =  heroes.filter(function(hero) {
    //     return hero.franchise == "test";
    // });
    return (
        <div  className="org-member-overview-report" >
            <Paper className=""> 
           <Grid container sm={12} spacing={2}>
               <Grid item sm={4} xs={4}>
                    { chartValue && (
                        <svg viewBox="0 0 36 36" className="circular-chart orange">
                            <path className="circle-bg"
                                d="M18 2.0845
                                a 15.9155 15.9155 0 0 1 0 31.831
                                a 15.9155 15.9155 0 0 1 0 -31.831"
                            />
                            <path className="circle"
                                stroke-dasharray={chartValue}
                                d="M18 2.0845
                                a 15.9155 15.9155 0 0 1 0 31.831
                                a 15.9155 15.9155 0 0 1 0 -31.831"
                            />
                        </svg>
                    )
                    }   
               </Grid>
               <Grid container sm={8} className="overview-chart-panel">
                   <label className="overview-title">ORG OVERVIEW</label>
                   <Grid item xs={6}>
    <span className="data-bold green">{activeCount}</span><label className="sub-caption">Active</label>
                   </Grid>
                   <Grid item xs={6}>
    <span className="data-bold red">{inActiveCount}</span><label className="sub-caption">Inactive</label>
                   </Grid>
               </Grid>
           </Grid>
           <Grid container sm={12}>
               <Grid className="btn-section" item  xs={12}>
                        <Button onClick={mangeOrgOverView} className="btn" variant="contained">Manage</Button>
               </Grid>
           </Grid>
           </Paper>
        </div>
    )
}
