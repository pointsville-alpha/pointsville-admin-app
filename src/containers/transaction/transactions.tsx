import React, { useState,useEffect } from 'react';
import { useHistory ,useParams} from "react-router-dom";

import MUIDataTable from "mui-datatables";
import {
    FormGroup,
    FormLabel,
    FormControl,
    ListItemText,
    TextField,
    Checkbox,
    FormControlLabel,
    Select,
    InputLabel,
    MenuItem
  } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import EmptyOrg from '../../assets/org-empty@2x.svg';
import SponsorEmty from '../../assets/sponsor-empty@2x.svg';

import { useQuery, gql } from '@apollo/client';
import Loader from '../../components/Loader/loader';


import Grid from '@material-ui/core/Grid';
import DropdownSelect from '../../components/Dropdown/dropdownselect';
import moment from 'moment';
import Button from "@material-ui/core/Button";
import profileImage from "../../assets/profile-img.png";
import '../../constant.css';
import './transactions.css';
import { createSecureContext } from 'tls';




  interface transactionInventory {
    id: string;
    sponsor_name: string;
    contact_name : string;
    contact_email : string;
}

interface transactionInventoryData {
    transactionInventory: transactionInventory[];
}

interface TransactionInventoryVars {
    year: number;
}
interface ParamTypes {
    orgId: string;
}

export const GET_TRANSACTION_QUERY = gql`
query getTransactionsByClub{
  getAllTransactions{
    id
    username  
    userProfilePic
    walletId
    orgName
    orgLogo
    price
    sponsorName
    sponsorLogo
    transactionDate
    transactionType
    points
  }
}
`;

export default function TransactionLists() {
        let { orgId } = useParams<ParamTypes>();
        const [transDate,setTransDate] = useState<any>("");
          
        const {refetch, loading, data } = useQuery<any>(GET_TRANSACTION_QUERY);  
  
        const [ageFilterChecked, setAgeFilterChecked] = React.useState(false);
        const [transfer,setTransfer] = useState(0);
        const [redeem,setRedeem] = useState(0);
        const [promotions,setPromotions] = useState(0);
        const [hunt,setHunt] = useState(0);


        useEffect(() => {
            refetch();
            if(!loading){
                //const tamil = 100;
                var transactions =  data.getAllTransactions.filter(function(transaction :any) { return transaction.transactionType == "PROMO";});
                //console.log(transactions);
                let promoTotal = transactions.reduce((total:any, current:any) => {return total + current.price },0);
                setPromotions(promoTotal);
                //console.log(result);
                var transactions =  data.getAllTransactions.filter(function(transaction :any) { return transaction.transactionType == "RECEIVED";});
                //console.log(transactions);
                let transferTotal = transactions.reduce((total:any, current:any) => {return total + current.price },0);
                setTransfer(transferTotal);
                var transactions =  data.getAllTransactions.filter(function(transaction :any) { return transaction.transactionType == "REDEEM";});
                //console.log(transactions);
                let redeemTotal = transactions.reduce((total:any, current:any) => {return total + current.price },0);
                setRedeem(redeemTotal);

            }
        },[data]);

    // const columns:any = [
    //         {
    //             name:"memberName",
    //             label:"Member Name",
    //             options:{
    //                 filter:false,
    //                 customBodyRender:(value:any, tableMeta:any, updateValue:any) => {
    //                     return (<div><img className='profile-img' src={value.profileImg} /><label className='membername'>{value.name}</label></div>)
    //                 }
    //             }
    //         },
    //         {
    //             name:"emailId",label:"Email Id",
    //             options:{filter:false}
    //         },
    //         {
    //              name:"phoneNumber",label:"PhoneNumber",
    //              options:{filter:false}
    //         },
    //         {
    //             name:"walletId",
    //             label:"Wallet Id",
    //             options:{filter:false}
    //          },
    //         {
    //             name:"walletPoints",
    //             label:"Wallet Points"
    //         },
    //         {
    //             name:"balance",
    //             label:"Balance"
    //         },
    //         {
    //             name: 'memberSince',
    //             label:"Member Since",
    //             options: {
    //               filter: true,
    //               filterType: 'custom',
        
    //               // if the below value is set, these values will be used every time the table is rendered.
    //               // it's best to let the table internally manage the filterList
    //               //filterList: [25, 50],
                  
    //               customFilterListOptions: {
    //                 render: (v:any) => {
    //                   if (v[0] && v[1] && ageFilterChecked) {
    //                     return [`Min Age: ${v[0]}`, `Max Age: ${v[1]}`];
    //                   } else if (v[0] && v[1] && !ageFilterChecked) {
    //                     return `Min Age: ${v[0]}, Max Age: ${v[1]}`;
    //                   } else if (v[0]) {
    //                     return `Min Age: ${v[0]}`;
    //                   } else if (v[1]) {
    //                     return `Max Age: ${v[1]}`;
    //                   }
    //                   return [];
    //                 },
    //                 update: (filterList:any, filterPos:any, index:any) => {
    //                   console.log('customFilterListOnDelete: ', filterList, filterPos, index);
        
    //                   if (filterPos === 0) {
    //                     filterList[index].splice(filterPos, 1, '');
    //                   } else if (filterPos === 1) {
    //                     filterList[index].splice(filterPos, 1);
    //                   } else if (filterPos === -1) {
    //                     filterList[index] = [];
    //                   }
        
    //                   return filterList;
    //                 },
    //               },
    //               filterOptions: {
    //                 names: [],
    //                 logic(age:any, filters:any) {
    //                     console.log(filters);
    //                     console.log(age);
    //                   if (filters[0] && filters[1]) {
    //                     return age < filters[0] || age > filters[1];
    //                   } else if (filters[0]) {
    //                     return age < filters[0];
    //                   } else if (filters[1]) {
    //                     return age > filters[1];
    //                   }
    //                   return false;
    //                 },
    //                 display: (filterList:any, onChange:any, index:any, column:any) => (
    //                   <div>
    //                     <FormLabel>Age</FormLabel>
    //                     <FormGroup row>
    //                       <TextField
                            
    //                         type="date"

    //                         value={filterList[index][0] || ''}
    //                         onChange={event => {
    //                             console.log(filterList[index]);
    //                           filterList[index][0] = event.target.value;
    //                           onChange(filterList[index], index, column);
    //                         }}
    //                         style={{ width: 'auto', marginRight: '3%' }}
    //                       />
    //                       <TextField
    //                         type="date"

    //                         value={filterList[index][1] || ''}
    //                         onChange={event => {
    //                           filterList[index][1] = event.target.value;
    //                           onChange(filterList[index], index, column);
    //                         }}
    //                         style={{ width: 'auto' }}
    //                       />
    //                       {/* <FormControlLabel
    //                         control={
    //                           <Checkbox
    //                             checked={ageFilterChecked}
    //                             onChange={event => setAgeFilterChecked(event.target.checked)}
    //                           />
    //                         }
    //                         label='Separate Values'
    //                         style={{ marginLeft: '0px' }}
    //                       /> */}
    //                     </FormGroup>
    //                   </div>
    //                 ),
    //               },
    //               print: false,
    //             },
    //           },
    //         // {
    //         //     name:"kycStatus",
    //         //     label:"KYC Status",
    //         //     options:{filter:false}
    //         // },
    //         {
    //             name:"status",
    //             label:"Status",
    //             options:{filter:false}
    //         }
    //         ];
      
    // const options:any = {
    //     filter: true,
    //     filterType: "dropdown",
    //     responsive:"standard",
    //     print:false,
    //     download : false,
    //     pagination : false,
    //     viewColumns : false,
    //     selectableRowsHideCheckboxes: true,
    //     fixedSelectColumn: false,
    //     selectToolbarPlacement: "none",
    //     selectableRowsHeader: false,
    //     selectableRows: "none",
    //     selectableRowsOnClick: false,
    //     fixedHeader : true,
    //     tableBodyHeight:'450px'
    //   }
    return (
    <div className="list-page transaction-list-wrapper">
        <h2 className="page-title">TRANSACTIONS</h2>
        <Grid container xs={12} sm={12}>
            <div>
                <Grid container className="item-status transaction-list-status" xs={12}>
                    {/* <Grid item sm={1} xs={2}>
                        <span className="data-bold pending">4000</span><label className="sub-caption">Pending</label>
                    </Grid>
                    <Grid item sm={1} xs={2}>
                        <span className="data-bold failed">500</span><label className="sub-caption">Failed</label>
                    </Grid> */}
                    <Grid item sm={1} xs={2}>
                        <span className="data-bold transfers">{transfer}</span><label className="sub-caption">Transfers</label>
                    </Grid>
                    <Grid item sm={1} xs={2}>
                        <span className="data-bold redeemed" >{redeem}</span><label className="sub-caption">Redeemed</label>
                    </Grid>
                    <Grid item sm={1} xs={2}>
                        <span className="data-bold hunt">0</span><label className="sub-caption">Hunt</label>
                    </Grid>
                    <Grid item sm={1} xs={2}>
                        <span className="data-bold promotions">{promotions}</span><label className="sub-caption">Promotions</label>
                    </Grid>
                </Grid>
            </div>
                <Grid  item sm={12} xs={12}>
                    <Paper className="table-wrapper"> 
                    <Grid container sm={12} className="mrgb-20">
                        <Grid item sm={8}>
                            <label className="table-heading">TRANSACTIONS LIST</label>
                        </Grid>
                        <Grid  item sm={4} className="select-with-label">
                            <label>Sort By</label>
                            <DropdownSelect/>
                        </Grid>
                    </Grid>
                     <TableContainer className="table-container" style={{ height: 450, width: '100%' }} component={Paper}>
                        <Table stickyHeader className="" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Organizations</TableCell>
                                    <TableCell align="left">Sponsors</TableCell>
                                    <TableCell align="left">Members</TableCell>
                                    <TableCell align="left">Wallet ID</TableCell>
                                    <TableCell align="left">Points</TableCell>
                                    <TableCell align="left">Transaction ID</TableCell>
                                    <TableCell align="left">Date & Time</TableCell>
                                    {/* <TableCell align="left">KYC</TableCell> */}
                                    {/* <TableCell align="left">Status</TableCell> */}
                                </TableRow>
                            </TableHead>
                            { loading ? ( 
                            <TableBody>
                                <TableCell colSpan={6}>
                                    <Loader size="60px"/>
                                </TableCell>
                            </TableBody>   
                            ): ( 
                            <TableBody>
                            {data.getAllTransactions.map((transaction:any) => { 
                                
                               return (
                                <TableRow >
                                  <TableCell className="label-parent" padding="none" >
                                    <img className="profile-img " src={transaction.orgLogo || EmptyOrg} />
                                    <label className="membername">{transaction.orgName}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <img className="profile-img " src={transaction.sponsorLogo || SponsorEmty}/>
                                    <label className="membername">{transaction.sponsorName}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <img className="profile-img " src={transaction.userProfilePic || "https://png.pngtree.com/png-vector/20190321/ourmid/pngtree-vector-users-icon-png-image_856952.jpg"} />
                                    <label className="membername">{transaction.username}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{transaction.walletId || "-"}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                      <label className="">{transaction.points}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                      <label className="">{transaction.id}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                  <label className="">{moment(parseInt(transaction.transactionDate)).format('DD MMM YYYY HH:mm')}</label>
                                </TableCell>
                                
                                {/* <TableCell className="label-parent" padding="none" >
                                    <label className="">Completed</label>
                                </TableCell> */}
                                </TableRow>
                            )})}
                            </TableBody>
                            )}
                        </Table>
                    </TableContainer>
                    {/* <div className="table-container muidata-table">
                        <MUIDataTable
                            title={"Transaction List"}
                            data={rows}
                            columns={columns}
                            options={options}
                        />
                    </div> */}
                </Paper>    
            </Grid>
        </Grid>
    </div>
    )
}
