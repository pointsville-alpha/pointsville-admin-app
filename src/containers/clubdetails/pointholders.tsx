import React from 'react';
import { useHistory ,useParams} from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import { useQuery, gql } from '@apollo/client';
import Loader from '../../components/Loader/loader';
import moment from 'moment';


import DropdownSelect from '../../components/Dropdown/dropdownselect';
import Button from "@material-ui/core/Button";
import '../../constant.css';
import '../transaction/transactions.css';


  interface pointHoldersInventory {
    id: string;
   
}

interface pointHoldersInventoryData {
    pointHoldersInventory: pointHoldersInventory[];
}

interface pointHoldersInventoryVars {
    year: number;
}
interface ParamTypes {
    orgId: string;
}

export const GET_POINTHOLDERS_QUERY = gql`
query getPointHolderLists($clubId : String!){
    getPointsHolders(clubId: $clubId){
      givenName
      email
      walletId
      profilePicture
      walletBalance
      walletId
      createdAt
    }
   }
`;

export default function PointsHolderList() {
   
    let { orgId } = useParams<ParamTypes>();
          
    const {refetch, loading, data } = useQuery<any>(
        GET_POINTHOLDERS_QUERY,{variables : {clubId : orgId}}
    ); 


    return (
    <div className="list-page transaction-list-wrapper">
        <Grid container xs={12} sm={12}>
                <Grid  item sm={12} xs={12}>
                    <Paper className="table-wrapper">
                    <Grid container className="table-head-section" sm={12}>
                        <Grid item sm={8}>
                            <label className="table-heading">POINT HOLDER LIST</label>
                        </Grid>
                        <Grid  item sm={4} className="select-with-label align-right flx-align-center flx-end">
                            <label className="select-label">Sort By</label>
                            <DropdownSelect/>
                            <button className="btn btn-primary add-btn display-none"><span className="btn-icon"></span><label className="btn-caption"> Add Org</label></button>
                        </Grid>
                    </Grid>
                    <TableContainer className="table-container" style={{ height: 450, width: '100%' }} component={Paper}>
                        <Table stickyHeader className="" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Point Holders</TableCell>
                                    <TableCell align="left">Email Id</TableCell>
                                    <TableCell align="left">Wallet ID</TableCell>
                                    <TableCell align="left">Points</TableCell>
                                    <TableCell align="left">Member Since</TableCell>
                                    {/* <TableCell align="left">KYC</TableCell> */}
                                </TableRow>
                            </TableHead> 
                            { loading ? (
                                <TableBody>
                                    <TableCell colSpan={6}>
                                        <Loader size="60px"/>
                                    </TableCell>
                                </TableBody>
                            ): (
                            <TableBody>
                            {data.getPointsHolders.map((pointHolders:any) => (
                                <TableRow >
                                <TableCell className="label-parent" padding="none" >
                                    <img className="profile-img " src={pointHolders.profilePicture || "https://png.pngtree.com/png-vector/20190321/ourmid/pngtree-vector-users-icon-png-image_856952.jpg"} />
                                    <label className="membername">{pointHolders.givenName}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{pointHolders.email}</label>
                                </TableCell>
                                
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{pointHolders.walletId || "-"}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{pointHolders.walletBalance || 0}</label>
                                </TableCell>
                               
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{moment(parseInt(pointHolders.createdAt)).format('D MMM YYYY HH:MM')}</label>
                                </TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                            )}
                        </Table>
                    </TableContainer>
                </Paper>    
            </Grid>
        </Grid>
    </div>
    )
}
