import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import Loader from '../../../components/Loader/loader';
import { useQuery, gql } from '@apollo/client';
import { useParams } from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Player1 from '../../../assets/Bitmap@2x.png';
import Player2 from '../../../assets/ic_chicago.svg';
import Video from '../../assets/profile_big_pic.png';
import '../../../constant.css';
import '../orginfo.css';


interface Props {
    orgCode ?:any
}

interface OrgsGamesInventory {

}

interface OrgsGamesInventoryData {
    orgsgameInventory: OrgsGamesInventory[];
}

interface OrgsGameInventoryVars {
}

interface ParamTypes {
    orgId: string;
}

const GET_GAME_BY_ID = gql`
  query getGameScheduleByOrgId($orgcode: String!) {
    getGameScheduleByOrgId(orgcode: $orgcode) {
        guid
        game_data
        orgcode

    }
  }
`;

const GET_ORG_BY_ID = gql`
  query getClub($clubId: String!) {
    getClub(clubId: $clubId) {
        orgCode
    }
  }
`;

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
  }
  
  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <div className="tabpanel-parent" >
            {children}
          </div>
        )}
      </div>
    );
  }
  
  function a11yProps(index: any) {
    return {
      id: `game-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
  function socialProps(index: any) {
    return {
      id: `social-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
export default function GameScheduler({orgCode}:Props) {
    const [gameTab, setGameTab] = React.useState(0);
    const [socialTab, setSocialTab] = React.useState(0);
    let { orgId } = useParams<ParamTypes>();
    const {refetch:re2, loading:lod2, data:dat2 } = useQuery<any>(GET_ORG_BY_ID,{variables : {clubId : orgId}});
    const {refetch:re1, loading:lod1, data:dat1 } = useQuery<any>(GET_GAME_BY_ID,{variables : {orgcode : dat2.getClub.orgCode}});

    console.log(dat1)

    const handleChangeGame = (event: React.ChangeEvent<{}>, newValue: number) => {
        setGameTab(newValue);
    };
    const handleChangeSocial = (event: React.ChangeEvent<{}>, newValue: number) => {
        setSocialTab(newValue);
    };
    return (
        <div className="container">
         { lod1 ? (
              <Loader size="30px"/>
              ) : (
              dat1.length>0 ?   
              dat1.getGameScheduleByOrgId.map((gameDetails:any) => {
              console.log(gameDetails.game_data)
              var parse = JSON.parse(gameDetails.game_data)
              console.log(parse)
              // console.log(JSON.parse(gameDetails.game_data));
              // console.log(JSON.parse(gameDetails.game_data))
                return(
                <div className="custom-tab org-info-games">
                <AppBar position="static">
                    <Tabs value={gameTab} onChange={handleChangeGame} aria-label="simple tabs example">
                    <Tab label="Today" {...a11yProps(0)} />
                    <Tab label="3 SEP" {...a11yProps(1)} />
                    <Tab label="4 SEP" {...a11yProps(2)} />
                    <Tab label="5 SEP" {...a11yProps(3)} />
                    <Tab label="6 SEP" {...a11yProps(4)} />
                    <Tab label="7 SEP" {...a11yProps(5)} />
                    <Tab label="8 SEP" {...a11yProps(6)} />
                    </Tabs>
                </AppBar>
                <TabPanel value={gameTab} index={0}>
                  <a href = {parse.gameUrl} target = "_blank">
                    <Grid container className="game match-details" sm={12}>
                        <Grid className="player-details" item sm={4}>
                            <img className="game-players" src={parse.TeamA.img}/>
                        <span className="player-name">{parse.TeamA.name}</span>
                        </Grid>
                        <Grid item sm={4}>
                            <div className="match">
                                <span className="match-type">- {parse.match} -</span>
                                <span>{parse.gameDate}</span>
                                {/* <span>9:00</span> */}
                            </div>
                            
                        </Grid>
                        <Grid className="player-details" item sm={4}>
                            <img className="game-players" src={parse.TeamB.img}/>
                            <span className="player-name">{parse.TeamB.name}</span>
                        </Grid>
                    </Grid>
                    </a>
                </TabPanel>
                <TabPanel value={gameTab} index={1}>
                    <Grid container className="game match-details" sm={12}>
                        <Grid className="player-details" item sm={4}>
                            <img className="game-players" src={Player1}/>
                            <span className="player-name">Chicago</span>
                        </Grid>
                        <Grid item sm={4}>
                            <div className="match">
                                <span className="match-type">- FINAL -</span>
                                <span>20 Aug</span>
                                <span>9:00</span>
                            </div>
                            
                        </Grid>
                        <Grid className="player-details" item sm={4}>
                            <img className="game-players" src={Player2}/>
                            <span className="player-name">Pirates</span>
                        </Grid>
                    </Grid>
                </TabPanel>
                <TabPanel value={gameTab} index={2}>
                <label>item1</label>
                </TabPanel>
                <TabPanel value={gameTab} index={3}>
                <label>item1</label>
                </TabPanel>
            </div>
        )}) : <p>No data found</p>
        
        )}
        </div>
    )
}
