import React, { useEffect, useState } from "react";
import Grid from '@material-ui/core/Grid';
import Loader from '../../../components/Loader/loader';
import { useQuery, gql } from '@apollo/client';
import { useParams } from "react-router-dom";
import profilePic from '../../../assets/profile_big_pic.png';

import '../../../constant.css';
import '../orginfo.css';


interface Props {
    orgCode?: any
}

interface OrgsEventInventory {

}

interface OrgsEventInventoryData {
    orgseventInventory: OrgsEventInventory[];
}

interface OrgsEventInventoryVars {
}

interface ParamTypes {
    orgId: string;
}

const GET_EVENT_BY_ID = gql`
  query getEventByOrgId($orgcode: String!) {
    getEventUriByOrgId(orgcode: $orgcode) {
        guid
        eventData
        orgcode

    }
  }
`;
const GET_ORG_BY_ID = gql`
  query getClub($clubId: String!) {
    getClub(clubId: $clubId) {
        orgCode
    }
  }
`;


export default function Event({ orgCode }: Props) {
    let { orgId } = useParams<ParamTypes>();
    const {refetch:re2, loading:lod2, data:dat2 } = useQuery<any>(GET_ORG_BY_ID,{variables : {clubId : orgId}});
    const {refetch:re1, loading:lod1, data:dat1 } = useQuery<any>(GET_EVENT_BY_ID,{variables : {orgcode : dat2.getClub.orgCode}});
    return (
        <div className="container">
            { lod1 ? (
                <Loader size="30px" />
            ) : (
                dat1.length > 0 ? (
                dat1.getEventUriByOrgId.map((eventDetails: any) => {
                        var data = eventDetails.eventData
                        console.log(data)
                        var parse = JSON.parse(data)
                        // console.log(parse.eventDate)
                       
                        // console.log(JSON.parse(gameDetails.game_data));
                        // console.log(JSON.parse(gameDetails.game_data))
                        return (
                            <div className="custom-tab org-info-games">

                                <Grid container sm={12}>
                                    <Grid className="events-sec mrgb-20" item sm={12}>
                                        <a href = {parse.eventUrl} target = "_blank">
                                        <img src={parse.imageUrl} alt="events" className="events-img" />
                                         <label className="event-title">{parse.eventName}</label>
                                         </a>
                                    </Grid>
                                </Grid>
                            </div>
                        )
                    })) : <p>No data found</p> 

                    )
                    
                    }
        </div>
    )
}