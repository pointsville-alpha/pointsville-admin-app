import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ProfileImage from './../../assets/who@2x.png';
import { useQuery, gql } from '@apollo/client';
import Loader from '../../components/Loader/loader';
import { useHistory ,useParams} from "react-router-dom";




import '../../constant.css';
import './sponsorList.css';

interface OrgsPlayerInventory {

}

interface OrgsPlayerInventoryData {
    orgsplayerInventory: OrgsPlayerInventory[];
}

interface OrgsPlayerInventoryVars {
}
interface ParamTypes {
    orgId: string;
}

export const GET_ORGPLAYERS_QUERY = gql`
query GetSponsorInventory ($orgcode: String!) {
    getPlayersByOrgId (orgcode: $orgcode){
    guid
    player_name
    player_no
    player_field
    player_bt
    player_ht
    player_wt
    player_dob
    playerUrl
  }
}
`;
const GET_ORG_BY_ID = gql`
  query getClub($clubId: String!) {
    getClub(clubId: $clubId) {
        orgCode
    }
  }
`;


function createData(promocodes: string, type: string, sponsors:string, points:string,availability: string, creationdate: string, validitydate: string,status:string) {
  return { promocodes, type, sponsors,points, availability, creationdate, validitydate, status };
}



export default function OrgPlayers() {
    let { orgId } = useParams<ParamTypes>();
    const {refetch:re2, loading:lod2, data:dat2 } = useQuery<any>(GET_ORG_BY_ID,{variables : {clubId : orgId}});
    const {refetch:re1, loading:lod1, data:dat1 } = useQuery<any>(GET_ORGPLAYERS_QUERY,{variables : {orgcode : dat2.getClub.orgCode}});

    console.log(dat1)
    console.log(dat2.getClub.orgCode)

    return (
                <div className="list-page org-payer-list">
                   
                 <TableContainer className="table-container" style={{ height: 450, width: '100%' }} component={Paper}>
                        <Table stickyHeader className="" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Players</TableCell>
                                    <TableCell align="left">Field</TableCell>
                                    <TableCell align="left">B/T</TableCell>
                                    <TableCell align="left">HT</TableCell>
                                    <TableCell align="left">WT</TableCell>
                                    <TableCell align="left">DOB</TableCell>
                                </TableRow>
                            </TableHead> 
                            {lod1 ? (
                                <TableBody>
                                    <TableCell colSpan={6}>
                                        <Loader size="60px"/>
                                    </TableCell>
                                </TableBody>
                             ) : ( 
                                
                            <TableBody>
                            {
                            dat1.getPlayersByOrgId.length>0 ? 
                            dat1.getPlayersByOrgId.map((sponsor : any) =>  (
                                <TableRow >
                                <TableCell className="label-parent" padding="none" >
                                <img className="profile-img " src={ProfileImage} />
                                    <label className="">{sponsor.player_name}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{sponsor.player_field}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{sponsor.player_bt}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{sponsor.player_ht}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{sponsor.player_wt}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{sponsor.player_dob}</label>
                                </TableCell>
                                </TableRow>
                            )) : 
                            <TableCell colSpan={6}>
                                <p>No data found</p>
                            </TableCell>
                            }
                            </TableBody>
                              )
                              } 
                        </Table>
                    </TableContainer>
            
    </div>
    )
}
