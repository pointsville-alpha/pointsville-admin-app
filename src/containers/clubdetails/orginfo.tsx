import React from 'react';
import { useHistory, useLocation, useParams } from "react-router-dom";
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import OrgPlayers from './orgplayers';
import NewsFeed from './newsfeed/newsfeed';
import { useQuery, gql } from '@apollo/client';
import VideoStreaming from './videostream/videostreaming'
import GameScheduler from './Games/games'
import Event from './event/event'
import '../../constant.css';
import './orginfo.css';




interface OrgInventory {
    id: string;
    name: string;
    orgCode: string;
}

interface OrgInventoryData {
    orgInventory: OrgInventory[];
}

interface OrgInventoryVars {
    clubId: string;
}




const GET_ORG_BY_ID = gql`
  query getClub($clubId: String!) {
    getClub(clubId: $clubId) {
        id
        name
        orgCode
        rate
        logoUrl
        availablePoints
        totalPoints
        fanCount
        newsApi
    }
  }
`;
interface ParamTypes {
    orgId: string;
}


interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
  }
  
  function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <div className="tabpanel-parent" >
            {children}
          </div>
        )}
      </div>
    );
  }
  
  function a11yProps(index: any) {
    return {
      id: `game-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
  function socialProps(index: any) {
    return {
      id: `social-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }
export default function OrgInfo() {
    
    const [gameTab, setGameTab] = React.useState(0);
    const [socialTab, setSocialTab] = React.useState(0);
    let { orgId } = useParams<ParamTypes>();
    const { loading, data } = useQuery<any>(
        GET_ORG_BY_ID,
        { variables: { clubId: orgId } }
    );

 
    const handleChangeGame = (event: React.ChangeEvent<{}>, newValue: number) => {
        setGameTab(newValue);
    };
    const handleChangeSocial = (event: React.ChangeEvent<{}>, newValue: number) => {
        setSocialTab(newValue);
    };
    return (
    <div className="details-page org-info-details">
        <Grid container xs={12} sm={12} spacing={2}>
            <Grid container sm={3} spacing={2}>
                <Grid item sm={12}>
                    <Paper className="games-wrapper mrgb-20">
                        <label className="sec-heading">Games</label>
                        <label className="sec-heading">Events</label>
                        { loading ? (
                           <p>...Loading</p>
                        ) : (
                        <GameScheduler orgCode="EventURI" /> 
                        )
                    }
                    </Paper>
                    <Paper className="videostreaming-wrapper">
                        <label className="sec-heading">Video Streaming</label>
                        { loading ? (
                           <p>...Loading</p>
                        ) : (
                        <VideoStreaming orgCode="ORG1" />
                        )
                    }
                    </Paper>
                </Grid>
                
            </Grid> 
            <Grid container sm={6} spacing={2}>
                <Grid item sm={12}>
                    <Paper className="social-wrapper mrgb-20">
                        <label className="sec-heading">Social Network Handles</label>
                        <div className="custom-tab org-info-games social-tab-container">
                            <AppBar position="static">
                                <Tabs value={socialTab} onChange={handleChangeSocial} aria-label="simple tabs example">
                                <Tab label="Facebook" {...socialProps(0)} />
                                <Tab label="Twitter" {...socialProps(1)} />
                                <Tab label="Instagram" {...socialProps(2)} />
                                </Tabs>
                            </AppBar>
                            <TabPanel value={socialTab} index={0}>
                                <Grid container className="tabpanel social-details" sm={12}>
                                    Facebook
                                </Grid>
                            </TabPanel>
                            <TabPanel value={socialTab} index={1}>
                                <Grid container className="tabpanel social-details" sm={12}>
                                    Twitter
                                </Grid>
                            </TabPanel>
                            <TabPanel value={socialTab} index={2}>
                                <Grid container className="tabpanel social-details" sm={12}>
                                    Instagram
                                </Grid>
                            </TabPanel>
                        </div>
                    </Paper>
                    <Paper>
                        <label className="sec-heading">Pirates Players</label>
                        <OrgPlayers/>
                    </Paper>
                </Grid>
            </Grid> 
            <Grid container sm={3} spacing={2}>
                <Grid item sm={12}>
                    <Paper className="event-wrapper padd-15 mrgb-20">
                        <label className="sec-heading">Events</label>
                        { loading ? (
                           <p>...Loading</p>
                        ) : (
                        <Event orgCode="ORG1" />
                        )

                    }
                    </Paper>
                    <Paper className="news-wrapper">
                        <label className="sec-heading">News</label>
                        { loading ? (
                           <p>...Loading</p>
                        ) : (
                            <NewsFeed newsFeedUri={data.getClub.newsApi}/>
                        )

                        }
                    </Paper>
                </Grid>
            </Grid>      
        </Grid>
    </div>
    )
}
