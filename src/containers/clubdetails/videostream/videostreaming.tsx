import React, { useEffect, useState } from "react";
import Axios from "axios";
import { Link, withRouter } from "react-router-dom";
import moment from "moment";
import Grid from '@material-ui/core/Grid';
import Loader from '../../../components/Loader/loader';
import { useQuery, gql } from '@apollo/client';

// import XMLParser from "react-xml-parser";
import Parser from 'rss-parser';

interface Props {
    orgCode ?:any
}

const GET_VIDEO_ORG_CODE = gql`
  query getVideoStreamingByOrgId($orgCode: String!) {
    getVideoStreamingByOrgId(orgCode: $orgCode) {
        videoUrl
        videoTitle
        videoDescription
        videoImg
    }
  }
`;
export default function VideoStreaming({orgCode}:Props) {
    const [feeds,setFeeds] = useState<any>([]);
    
    const { loading, data } = useQuery<any>(
        GET_VIDEO_ORG_CODE,
        { variables: { orgCode: "ORG1" } }
    );
    useEffect(() => {
        console.log("useEffect");
    },[]);

    return (
        <div className="container">
         { loading ? (
              <Loader size="30px"/>
              ) : (

        data.getVideoStreamingByOrgId.map((videoDetails:any) =>{
            return (
            <Grid container className="video-streaming-sec" sm={12} spacing={2}>
                <Grid item className="video-player" sm={5}>
                    <a href={videoDetails.videoUrl} target="blank" className="link">
                        <img src={videoDetails.videoImg} className="video-player-thumbnail" alt="video"/>
                    </a>
                </Grid>
                <Grid className="video-details" item sm={7}>
                    <a href={videoDetails.videoUrl} target="blank"  className="link">
                        <label className="video-title">{videoDetails.videoTitle}</label>
                        <p className="video-description">{videoDetails.videoDescription}</p>
                    </a>
                </Grid>
            </Grid>
        )}))}
        </div>
    )
}

