import React, { useEffect, useState } from "react";
import Axios from "axios";
import { Link, withRouter } from "react-router-dom";
import moment from "moment";
import Grid from '@material-ui/core/Grid';
import Loader from '../../../components/Loader/loader';
// import XMLParser from "react-xml-parser";
import Parser from 'rss-parser';

interface Props {
    newsFeedUri ?:any
}
export default function NewsFeed({newsFeedUri}:Props) {
    const [feeds,setFeeds] = useState<any>([]);
    const [loading,setLoading] = useState<any>(true);
    console.log(newsFeedUri);

    useEffect(() => {
        console.log("useEffect");
        let parser = new Parser();
        const CORS_PROXY = "https://cors-anywhere.herokuapp.com/"
        parser.parseURL(CORS_PROXY+newsFeedUri,function(err,feed){
            if (err){ 
                setLoading(false);
                throw console.log(err); 
            }
            console.log(feed.items);
            setFeeds(feed.items);
            setLoading(false);
        });
        console.log(feeds);
    },[]);
    return (
        <div className="container">
         { loading ? (
              <Loader size="30px"/>
              ) : (
                feeds.length > 0 ? (
                feeds.map((feed:any) => (
                    <Grid container className="video-streaming-sec" sm={12} spacing={2}>
                        {/* <Grid item className="video-player" sm={5}>
                            <img className="video-player-thumbnail" />
                            {/* <video width="400" controls preload="metadata">
                                <source src="https://www.w3schools.com/html/mov_bbb.mp4#t=0.5" type="video/mp4" />
                            </video> 
                        </Grid> */}
                        <Grid className="video-details" item sm={12}>
                            <a className="link news-feed" href={feed.link} target="blank">
                                <label className="video-title">{feed.creator}</label>
                                <p className="video-description">{feed.title}</p>
                            </a>
                        </Grid>
                    </Grid>
                )) 
                ):( <p> No data found</p>) 
        
        )}
        </div>
        )
}

