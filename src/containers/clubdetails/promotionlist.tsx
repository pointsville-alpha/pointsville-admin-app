import React,{useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory ,useParams} from "react-router-dom";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import DropdownSelect from '../../components/Dropdown/dropdownselect';
import CreatePromotions from '../createpromotions/createpromotions';
import { useQuery, gql } from '@apollo/client';
import '../../constant.css';
import moment from 'moment';
import './sponsorList.css';
import Loader from '../../components/Loader/loader';
import Pirates from '../../assets/Pirates.png';
import Pnc from '../../assets/PNC.png';
import Highmark from '../../assets/Highmark.png';
import Nasdaq from '../../assets/Nasdaq.png';
import Van from '../../assets/van.png';
import Veterans from '../../assets/Veterans.png';

interface PromotionInventory {
    id: string;
    promoType: string;
}

interface ParamTypes {
    orgId: string;
}
interface PromotionInventoryData {
    promoInventory: PromotionInventory[];
}

interface PromoInventoryVars {
    year: number;
}


  export const GET_PROMO_QUERY = gql`
    query GetPromotionsData( $orgId : String!) {
        getPromoWithOrgId (orgId : $orgId){
            promoCodePrefix
            promoCodePoints
            promoType
            validity
            promoTotalCount
            sponsorId
            id
            pointsAvailable
            noPointsPerPromo
            sponsorLogo
            createdAt
        }
    }
`;

export default function PromotionLists() {
   
    const [open, setOpen] = React.useState(false);
    const [promotionId, setPromotionId] = React.useState<any>();
    let { orgId } = useParams<ParamTypes>();

    const { loading, data ,refetch} = useQuery<any>(
        GET_PROMO_QUERY , {variables : { orgId : orgId}}
    );
    const handleClickOpen = () => {
        setOpen(true);
        setPromotionId(false);
    };

    useEffect(() => {
        console.log("useEffect");
        refetch();
    },[open]);

    const handleClose = () => {
        setOpen(false);
        //setTimeout (() => {refetch()},1000);
    };
    const handleEditClickOpen = (id:string) =>{
        setOpen(true);
        setPromotionId(id);
    };


    return (
    <div className="list-page transaction-list-wrapper">
        <Grid container xs={12} sm={12} spacing={2}>
               
                <Grid  item sm={12} xs={12}>
                    <Paper className="table-wrapper">
                    <Grid container className="table-head-section" sm={12}>
                        <Grid item sm={8}>
                            <label className="table-heading">PROMOTIONS LIST</label>
                        </Grid>
                        <Grid  item sm={4} className="select-with-label align-right flx-align-center flx-end">
                            <label className="select-label">Sort By</label>
                            <DropdownSelect/>
                            <button onClick={handleClickOpen} data-view="Create" className="btn btn-primary add-btn"><span data-view="Create" className="btn-icon"></span><label className="btn-caption">Add Promotion</label></button>
                        </Grid>
                    </Grid>
                    <TableContainer className="table-container" style={{ height: 450, width: '100%' }} component={Paper}>
                        <Table stickyHeader className="" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Promocodes</TableCell>
                                    <TableCell align="left">Type</TableCell>
                                    <TableCell align="left">Sponsors</TableCell>
                                    <TableCell align="left">Points per promo</TableCell>
                                    <TableCell align="left">Availability</TableCell>
                                    <TableCell align="left">Creation Date</TableCell>
                                    <TableCell align="left">Validity Date</TableCell>
                                    <TableCell align="left">Status</TableCell>
                                    <TableCell align="left"></TableCell>
                                </TableRow>
                            </TableHead> 
                                { loading? (
                                   <TableBody>
                                        <TableCell colSpan={6}>
                                            <Loader size="60px"/>
                                        </TableCell>
                                    </TableBody>        
                                ):(     
                                    <TableBody>
                                    {data.getPromoWithOrgId.map((promo:any) => (
                                        <TableRow className="pointer"  onClick={() => handleEditClickOpen(promo.id)} data-id= {promo.id} data-view="Edit" >
                                        <TableCell className="label-parent " padding="none" >
                                            <label className="promocode pointer">{promo.promoCodePrefix}  {promo.promoCodePoints}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <label className="">{promo.promoType}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <img className="profile-img promo-list" src={promo.sponsorLogo || "https://us.v-cdn.net/6022045/uploads/defaultavatar.png"} />
                                            <label className="">{}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <label className="">{promo.promoCodePoints}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <label className="">{promo.pointsAvailable +"/"+ promo.promoTotalCount}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <label className="">{moment(parseInt(promo.createdAt)).format('MM/DD/YYYY')}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <label className="">{moment(parseInt(promo.validity)).format('MM/DD/YYYY')}</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                            <label className="green">Active</label>
                                        </TableCell>
                                        <TableCell className="label-parent" padding="none" >
                                           
                                        </TableCell>
                                        
                                        </TableRow>
                                    ))}   
                                    </TableBody>
  
                                )}
                            
                        </Table>
                    </TableContainer>
                </Paper>    
            </Grid>
        </Grid>
        
        <CreatePromotions onOpen ={open} promoId= {promotionId}  handleCloseClick={handleClose}/>
    </div>
    )
}
