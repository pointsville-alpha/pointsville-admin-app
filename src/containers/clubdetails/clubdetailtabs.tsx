import React from 'react';
import { RouteProps,useLocation ,RouteComponentProps} from "react-router";

import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Paper from '@material-ui/core/Paper';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import PointHolderList from './pointholders';
import PromotionLists from './promotionlist';
import SponsorList from './sponsorList';
import OrgInfo from './orginfo';
import './clubdetailtabs.css';


interface Props {
  onParentRender: () => void,
}
interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}
interface LocationState extends RouteComponentProps {
  state ?: string;
}


function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <div className="tabpanel-parent" >
          {children}
        </div>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function ClubDetailsTabs({onParentRender} : Props) {
  const {state} = useLocation<LocationState>();


  console.log(state);
  
  const tabValue = (state)?1:0;
  //console.log(tabValue);
  const [value, setValue] = React.useState(tabValue);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  return (
    <div className="custom-tab org-info-main">
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Sponsor" {...a11yProps(0)} />
          <Tab label="Promotions" {...a11yProps(1)} />
          <Tab label="Point Holders" {...a11yProps(2)} />
          <Tab label="Org Info" {...a11yProps(3)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
          <SponsorList onParentRender = {onParentRender}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
            <PromotionLists/>
      </TabPanel>
      <TabPanel value={value} index={2}>
            <PointHolderList/>
      </TabPanel>
      <TabPanel value={value} index={3}>
            <OrgInfo/>
      </TabPanel>
    </div>
  );
}
