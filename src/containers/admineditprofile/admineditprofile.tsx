import React, { useState,useEffect } from 'react';
import { RouteProps,useLocation ,RouteComponentProps,useHistory} from "react-router";
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import DoneIcon from '@material-ui/icons/Done';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ReactCrop from 'react-image-crop';
import { Auth } from 'aws-amplify';
import { useQuery, gql, useMutation } from '@apollo/client';


import '../../constant.css';
import '../memberprofile/memberprofile.css';
import '../adminprofiledetails/adminprofiledetails.css';
import ProfileImg from '../../assets/profile_big_pic.png';

interface IFormUser {
    givenName: string;
    email: string;
    phoneNumber: string;
    
}
interface LocationState extends RouteComponentProps {
    state ?: string;
  }

interface TestState {
    crop?: ReactCrop.Crop;
}

const initialState = {
    crop: {
        x: 100,
        y: 200
    }
};
interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}

let defaultValues:any = {
    givenName:"",
    email:"",
    phoneNumber:""
}
const UPDATE_MEMBER = gql`
mutation updateUserDetails($username : String!,$data: UpdatedUserInput! ){
    updateUserDetails(username: $username, data: $data ){
        givenName
    }
   }`;
const GET_USER_BY_ID = gql`
  query getUsers($userId: String!) {
    getUserWithId(userId: $userId) {
        id
        username
        givenName
        email
        dob
        address
        phoneNumber
        isActive

    }
  }
`;

export default function AdminEditProfile(Test:TestState) {
    const history = useHistory();
    const {state} = useLocation<LocationState>();

    const cancelClick = () => history.push({pathname:'/profiledetails',state:state});
    const { handleSubmit, control, reset, errors,setValue } =  useForm<IFormUser>({defaultValues,shouldUnregister:false});
   
    const [updateUserDetails] = useMutation(UPDATE_MEMBER,{ onCompleted : () =>  history.push({pathname:'/profiledetails',state:state}) });
    const onSubmit: SubmitHandler<IFormUser> = data => {
        //updateUser(data); 
        updateUserDetails({
            variables : 
            { username : state,
                data : 
                { 
                    givenName : data.givenName,
                    email : data.email,
                    username:state,
                    phoneNumber : data.phoneNumber
                }
            }
        })
    }
    const { loading, data,refetch } = useQuery<any>(GET_USER_BY_ID, { variables: { userId: state } });
    
    useEffect(() => {
        console.log("use Effect");
            if(!loading){
                console.log(data);
                //setMemberDeails(data.getMemberWithId);
                const { givenName,email,phoneNumber} = data.getUserWithId;
                defaultValues = {givenName,email,phoneNumber};
                setValue("givenName",defaultValues.givenName);
                setValue("phoneNumber",defaultValues.phoneNumber);
                setValue("email",defaultValues.email);
               
            }
    },[data,setValue]);



    const [crop,setCrop] = useState<any>(initialState);
    const [upImg,setUpImg] = useState<any>();
    //state = initialState;

    const onChange = (crop: ReactCrop.Crop) => {
        setCrop(crop);
    }
    // const onSelectFile = (e?: React.ChangeEvent<HTMLInputEvent> ) => {
    //     let files:any = (<HTMLInputElement>e.target).files;
    //     if (files && files.length > 0) {
    //       const reader = new FileReader();
    //       reader.addEventListener("load", () => setUpImg(reader.result));
    //       reader.readAsDataURL(files[0]);
    //     }
    //   };
    // handleChangeFile(selectorFiles: FileList)
    // {
    //     console.log(selectorFiles);
    // }
   

    return (
    <div className="detail-page admin-profile-details">
        <div className="page-head p-relative padl-40">
            
        </div>
        <div className="page-body padl-30" >
            <Grid container spacing={3} sm={12}>
                <Grid item className="mrgCenter" sm={6} spacing={1}>
                    <div className="profile-img-section edit-profile-details align-center" style={{backgroundImage:`url(${ProfileImg})`}}>
                        <span className="profile-photo-icon"></span>
                        <ReactCrop src={upImg} onChange={onChange} crop={{x: 100,y: 200}} />
                        {/* <div>
        <input type="file" accept="image/*" onChange={ (e) => handleChangeFile(e.target.files) } />
      </div> */}
                    </div>
                    <Paper className="">
                        <Grid container className="profile-details flx-align-center" sm={12} spacing={0}>
                            <Grid item sm={12} xs={12}>
                                <h5 className="profile-name">Personal Info</h5>
                            </Grid>
                        </Grid>    
                        <div className="admin-personal-info-section edit-container">
                            <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
                            <Grid item sm={12}>
                                <div className="input-group">
                                    <label className="input-label" >Full Name*</label>
                                    <Controller as={TextField}  rules={{ required: true}} defaultValue = ""  className={`form-fields  ${errors.givenName && 'error'}`} type="text" name="givenName" placeholder="Full Name" control={control} />                            
                                    {errors.givenName && <label className="error-message">Field is required</label>}
                                </div>
                            </Grid>
                            <Grid item sm={12}>
                                 <div className="input-group">
                                    <label className="input-label" >Email*</label>
                                    <Controller as={TextField}  type="email" name="email" disabled placeholder="Email" defaultValue ="" rules={{ required: true,pattern :/^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/}} className={`form-fields  ${errors.email && 'error'}`} control={control} />                            
                                    {errors.email && errors.email.type == "required" && (<label className="error-message">Field is required</label>)}
                                    {errors.email && errors.email.type == "pattern" && (<label className="error-message">Field should be in mail format</label>)}
                                </div>
                            </Grid>
                            <Grid item sm={12}>
                                 <div className="input-group">
                                    <label className="input-label" >Phone Number*</label>
                                    <Controller as={TextField}  type="text" disabled rules={{ required: true, pattern:/[0-9]/ }} defaultValue ="" name="phoneNumber" placeholder="Contact Phone Number"  className={`form-fields  ${errors.phoneNumber && 'error'}`} control={control}  />                            
                                    {errors.phoneNumber && errors.phoneNumber.type == "required" && (<label className="error-message">Field is requried</label>)}
                                    {errors.phoneNumber && errors.phoneNumber.type == "pattern" && (<label className="error-message">Field should be in number</label>)}
                                </div>
                            </Grid>
                            <Grid item className="align-right" sm={12}>
                                <Button onClick={cancelClick} className="btn btn-secondary mrgl-30 btn-padding" color="primary">
                                    CANCEL
                                </Button>
                                <Button color="primary" className="btn btn-primary button mrgl-30 btn-padding" onClick={handleSubmit(onSubmit)}>
                                    SAVE   
                                </Button>
                            </Grid>
                            
                            </form>
                        </div>
                    </Paper>
                </Grid>
            </Grid>
            
        </div>
    </div>
    )
}
