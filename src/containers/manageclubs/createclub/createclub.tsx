import React, {useEffect, useState} from 'react'
import { useHistory } from "react-router-dom";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Loader from '../../../components/Loader/loader';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import '../../../constant.css';
import '../manageclubs.css';
import OrgImage from '../../../assets/Bitmap@2x.png';
import EmptyOrg from '../../../assets/org-empty@2x.svg';

import { gql, useMutation,useQuery } from '@apollo/client';
// import { CREATE_ORG } from '../../../graphql';
import S3 from 'aws-s3';
import { s3config } from "../../../s3config";
import { stringify } from 'querystring';
 

interface Props {
    onOpen : boolean,
    handleCloseClick: () => void,
    editData?: string,
    editId?: string

}
interface IFormAddClub {
    orgLogo: string;
    name: string;
    orgCode: string;
    rate: number;
    rosterApi : string;
    scheduleApi : string;
    videoApi : string;
    eventsApi : string;
    newsApi : string;
    facebook : string;
    instagram : string;
    twitter : string;
}

let defaultValues:any = {
    orgLogo: "",
    name: "",
    orgCode: "",
    rate: 0,
    rosterApi : "",
    scheduleApi : "",
    videoApi : "",
    eventsApi : "",
    newsApi : "",
    facebook : "",
    instagram : "",
    twitter : "",
}

const UPDATE_ORG = gql`
mutation updateOrgs($id : String!,$data: UpdateClubInput! ){
    updateOrg(id: $id, data: $data ){
      name
     orgCode
    }
   }`;


const CREATE_ORG = gql`
  mutation createOrgs($data: NewClubInput! ) {
    createOrg(data: $data) {
      name,
      orgCode,
      rate
    }
  }
`;
const GET_ORG_BY_ID = gql`
  query getClub($clubId: String!) {
    getClub(clubId: $clubId) {
      id
      name
      orgCode
      logoUrl
      rate
      rosterApi
      scheduleApi
      videoApi
      eventsApi
      newsApi
      facebook
      instagram
      twitter

    }
  }
`;

export default function CreateClub({handleCloseClick,onOpen,editData,editId}:Props) {
    const open = onOpen;
    const orgId = editId?editId:"";
    //const methods = useForm<IFormAddClub>({});
    //const { handleSubmit, control, reset,errors, formState,setValue } = methods;
    const [orgInfo ,setOrgInfo] = useState<any>({});
    const [logoUrl, setlogoUrl] = useState<any>();
    const { loading, data,refetch } = useQuery<any>(GET_ORG_BY_ID, { variables: { clubId: orgId } });
    
    const { register, handleSubmit, errors,control, reset, formState, setValue} = useForm<IFormAddClub>({defaultValues,shouldUnregister:false});
    const S3Client = new S3(s3config);

    const fieldSetValueFunction = (defaultValue:any) =>{
            setlogoUrl(defaultValues.logoUrl);
            setValue("name",defaultValues.name);
            setValue("orgCode",defaultValues.orgCode);
            // setValue("orgLogo",defaultValues.logoUrl);
            setValue("rate",defaultValues.rate);
            setValue("rosterApi",defaultValues.rosterApi);
            setValue("scheduleApi",defaultValues.scheduleApi);
            setValue("videoApi",defaultValues.videoApi);
            setValue("eventsApi",defaultValues.eventsApi);
            setValue("newsApi",defaultValues.newsApi);
            setValue("facebook",defaultValues.facebook);
            setValue("instagram",defaultValues.instagram);
            setValue("twitter",defaultValues.twitter);
    }
    
    useEffect(() => {
        refetch();
        if(!loading && editId){
            defaultValues = {...data.getClub}; 
            fieldSetValueFunction(defaultValues);
        }else{
            defaultValues = {
                orgLogo: "",
                name: "",
                orgCode: "",
                rate: 0,
                rosterApi : "",
                scheduleApi : "",
                videoApi : "",
                eventsApi : "",
                newsApi : "",
                facebook : "",
                instagram : "",
                twitter : "",
            }
            fieldSetValueFunction(defaultValues);
        }
    },[data,open,setValue])
    
    const [createOrgs] = useMutation(CREATE_ORG,{
        // after updating the post, we go to the home page
        onCompleted: () => handleCloseClick(),
      });
    const [updateOrgs] = useMutation(UPDATE_ORG,{
        // after updating the post, we go to the home page
        onCompleted: () => handleCloseClick(),
      });
    const onSubmit: SubmitHandler<any> = data =>
    {
        if (orgId) {
            updateOrgs({variables: 
                    {
                        id : orgId,
                        data: 
                        {
                            name: data.name,
                            rate: parseFloat(data.rate),
                            orgCode: data.orgCode,
                            logoUrl: logoUrl,
                            rosterApi:data.rosterApi,
                            scheduleApi:data.scheduleApi,
                            videoApi : data.videoApi,
                            eventsApi : data.eventsApi,
                            newsApi : data.newsApi,
                            facebook : data.facebook,
                            instagram :data.instagram,
                            twitter  : data.twitter    
                        }
                    }
                });
        } else {
        createOrgs({variables: 
            {
                data: 
                {
                    name: data.name,
                    rate: parseFloat(data.rate),
                    orgCode: data.orgCode,
                    logoUrl: logoUrl,
                    rosterApi:data.rosterApi,
                    scheduleApi:data.scheduleApi,
                    videoApi : data.videoApi,
                    eventsApi : data.eventsApi,
                    newsApi : data.newsApi,
                    facebook : data.facebook,
                    instagram :data.instagram,
                    twitter  : data.twitter   
                }
            }
        });
    }
        //handleCloseClick();
    }
    const [loader,setLoader] = useState<any>(false);

    const uploadImage = (e:React.ChangeEvent<any>) =>{
        const targetFiles = e.target.files;
        console.log(targetFiles);
        setLoader(true);
        if(targetFiles.length>0){
            S3Client
            .uploadFile(targetFiles[0], `newFileName${Math.random()}`)
            .then((res: any) => { 
                console.log(res.location);
                setlogoUrl(res.location);
                setLoader(false);
            })
            .catch((err: any) => console.error(err));
        }
    }

    const handleInActiveClick = (orgId:any) => {
        if(orgId){
            updateOrgs({variables: 
                {
                    id : orgId,
                    data: 
                    {
                        isActive:false
                    }
                }
            });
        }
    }
    return (
        <Dialog open={onOpen} onClose={handleCloseClick} aria-labelledby="form-dialog-title" className="common-dialog-container org-container">
            <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
                <DialogTitle id="form-dialog-title">{editId ? "Edit" : "Add" } organization</DialogTitle>
                <DialogContent>
                     <Grid className="form-grid" container sm={12} spacing={6}>
                            <Grid item sm={4} xs={12}>
                                
                                <div className="input-group input-file-group edit">
                                    {loader && <Loader size="30px"/>}
                                    <img className="profile-img-editview" src={logoUrl || EmptyOrg} alt="org-logo"/>
                                    <input ref={register} type="file" name="orgLogo" onChange={(e) => uploadImage(e)} className="form-fields form-field-file" />                            
                                    <div className="file-mask-section edit-view">
                                        <span className="upload-img"></span>
                                        <label>upload Logo</label>
                                        
                                    </div>
                                </div>
                                <div className="input-group">
                                    <label className="input-label">Organization Name*</label>
                                    <Controller as={TextField}  rules={{ required: true,pattern:/[A-Za-z]/ }} type="text" name="name" placeholder="Organization Name" className={`form-fields  ${errors.name && 'error'}`} control={control}  />                            
                                    {errors.name && <label className="error-message">Field is required</label>}
                                </div>
                                <div className="input-group">
                                    <label className="input-label">Organization Code*</label>
                                    <Controller as={TextField}  type="text" rules={{ required: true, validate : (value) =>value.length<=6 && value.length>=3 }} name="orgCode" placeholder="Organization Code" className={`form-fields  ${errors.orgCode && 'error'}`} control={control}  />                            
                                    {errors.orgCode && errors.orgCode.type == "required" && (<label className="error-message">Field is requried</label>)}
                                    {errors.orgCode && errors.orgCode.type == "validate" && (<label className="error-message">Code should be Min 3 and Max 5 Characters</label>)}                                </div>  
                                <div className="input-group">
                                    <label className="input-label">Rate per Point(USD)*</label>
                                    <Controller as={TextField}  type="number" rules={{ required: true, pattern:/[0-9]/ }} name="rate"   placeholder="Organization Rate" className={`form-fields  ${errors.rate && 'error'}`}  control={control} />                            
                                    {errors.rate && errors.rate.type == "required" && (<label className="error-message">Field is requried</label>)}
                                    {errors.rate && errors.rate.type == "pattern" && (<label className="error-message">Field should be in number</label>)}
                                </div>
                            </Grid>
                            <Grid item className="borderL padr0" sm={8} xs={12}>
                                <h5 className="section-heading form-heading mrg0">Organization Info</h5>
                                <div className="input-group">
                                    <label className="input-label" >Player Roster</label>
                                    <Controller as={TextField}  type="text" name="rosterApi" placeholder="API Information" className="form-fields" control={control} defaultValue="" />                            
                                </div>
                                <div className="input-group">
                                    <label className="input-label" >Game Schedule</label>
                                    <Controller as={TextField}  type="text" name="scheduleApi" placeholder="API Information" className="form-fields" control={control} defaultValue="" />                            
                                </div>
                                <div className="input-group">
                                    <label className="input-label" >Video Streaming</label>
                                    <Controller as={TextField}  type="text" name="videoApi" placeholder="API Information" className="form-fields" control={control} defaultValue="" />                            
                                </div>
                                <div className="input-group">
                                    <label className="input-label" >Events</label>
                                    <Controller as={TextField}  type="text" name="eventsApi" placeholder="API Information" className="form-fields" control={control} defaultValue="" />                            
                                </div>
                                <div className="input-group">
                                    <label className="input-label" >News</label>
                                    <Controller as={TextField}  type="text" name="newsApi" placeholder="API Information" className="form-fields" control={control} defaultValue="" />                            
                                </div>
                                <div className="input-group social-url-container">
                                    <label className="input-label" >Socialnetwork Handles</label>
                                    <Controller as={TextField}  type="text" name="facebook" placeholder="URL Information" className="form-fields social-field fb" control={control} defaultValue="" />                            
                                    <Controller as={TextField}  type="text" name="instagram" placeholder="URL Information" className="form-fields social-field insta" control={control} defaultValue="" />                            
                                    <Controller as={TextField}  type="text" name="twitter" placeholder="URL Information" className="form-fields social-field twitter" control={control} defaultValue="" />                            
                                </div>
                            </Grid>
                            
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        {editId &&
                        <Button onClick={() => handleInActiveClick(data.getClub.id)} className="btn btn-secondary btn-delete" color="primary">
                            DEACTIVATE
                        </Button>
                        }
                        <Button onClick={handleCloseClick} className="btn btn-secondary" color="primary">
                            CANCEL
                        </Button>
                        <Button color="primary" className="btn btn-primary button" onClick={handleSubmit(onSubmit)}>
                            {editId? "EDIT ORG":"ADD ORG"}   
                        </Button>
                    </DialogActions>
                </form>
        </Dialog>
    )
}
