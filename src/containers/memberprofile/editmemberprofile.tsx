import React ,{useState,useEffect}from 'react'
import { useHistory } from "react-router-dom";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import moment from 'moment';
import ReactDatePicker from "react-datepicker";
import '../../constant.css';
import '../manageclubs/manageclubs.css';
import { useQuery, gql, useMutation } from '@apollo/client';

//import OrgImage from '../../../assets/Bitmap@2x.png';
interface Props {
    onOpen : boolean,
    handleCloseClick: () => void,
    memberId:string,
}

interface IFormAddClub {
    
    givenName: string;
    phoneNumber: string;
    address: string;
    dob : string;
    email : string;
}

let defaultValues:any = {
    givenName: "",
    phoneNumber: "",
    address:"",
    dob:"10/10/2200",
    email:""
  };

  const GET_USER_BY_ID = gql`
  query getUsers($userId: String!) {
    getUserWithId(userId: $userId) {
        id
        username
        givenName
        email
        dob
        address
        phoneNumber
        isActive

    }
  }
`;

const UPDATE_MEMBER = gql`
mutation updateUserDetails($username : String!,$data: UpdatedUserInput! ){
    updateUserDetails(username: $username, data: $data ){
        username
    }
   }`;

export default function EditMemberProfile({handleCloseClick,onOpen,memberId}:Props) {
    const userId = memberId?memberId:"";
    const openState = onOpen;
    const { loading, data,refetch } = useQuery<any>(GET_USER_BY_ID, { variables: { userId: userId } });
    //console.log(data);
    const history = useHistory();
    const backClick = () => history.push('/managemembers');
    const [open, setOpen] = React.useState(false);
    const { handleSubmit, control,setValue, reset,register } = useForm<IFormAddClub>({defaultValues,shouldUnregister:false});
    useEffect(() => {
       //setTimeout(() => { 
        if(!loading){
            console.log(data);
            //setMemberDeails(data.getMemberWithId);
            const { givenName,email,phoneNumber,dob,address } = data.getUserWithId;
            defaultValues = {givenName,email,phoneNumber,dob,address};
            let dateConversion = moment(defaultValues.dob).format("YYYY-MM-DD");
            console.log(defaultValues);
            setValue("givenName",defaultValues.givenName);
            setValue("phoneNumber",defaultValues.phoneNumber);
            setValue("email",defaultValues.email);
            setValue("dob", dateConversion);
            setValue("address",defaultValues.address);
        }
    //})
    },[data,openState,setValue])

    const [updateUserDetails] = useMutation(UPDATE_MEMBER,{ onCompleted : () => { 
        history.push('/managemembers'); 
        handleCloseClick();
    }
    });

    const onSubmit: SubmitHandler<IFormAddClub> = data => {
        if(userId){
            updateUserDetails({
                variables : 
                { username : userId,
                    data : 
                    { 
                        givenName : data.givenName,
                        address : data.address,
                        username:userId,
                        email : data.email,
                        dob :new Date(data.dob),
                        phoneNumber : data.phoneNumber

                    }
                }
            })
        }
    }
    const handledeactiveClick = () => {    
        if(userId){
            updateUserDetails({
                variables : 
                { username : userId,
                    data : 
                    { 
                        isActive : false
                    }
                }
            })
        }
    }

    return (
        <Dialog open={onOpen} onClose={handleCloseClick} aria-labelledby="form-dialog-title" className="common-dialog-container member-container">
                <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
                   
                <DialogContent>
                        <h5 id="form-dialog-title" className="section-heading">Edit Member</h5>
                        <Grid className="form-grid" container sm={12} spacing={2}>
                            <Grid item sm={12} xs={12}>
                                <h5 className="section-heading form-heading mrg0">Organization Info</h5>
                                </Grid>
                            <Grid item sm={6} xs={12}>
                                <div className="input-group">
                                    <label className="input-label" >Full Name</label>
                                    <Controller as={TextField}  type="text" name="givenName" defaultValue="" placeholder="Full Name" className="form-fields" control={control}/>                            
                                </div>
                                <div className="input-group">
                                    <label className="input-label" >Contact Phone</label>
                                    <Controller as={TextField} disabled type="text" name="phoneNumber" defaultValue="" placeholder="Contact Phone number" className="form-fields" control={control}  />                            
                                </div>
                                <div className="input-group textarea-group">
                                    <label className="input-label" >Address</label>
                                    <Controller as={TextareaAutosize} aria-label="minimum height" defaultValue="" rowsMin={3} placeholder="Address"   name="address"  className="form-fields textarea-field MuiInputBase-input MuiInput-input" control={control}   />                            
                                </div>
                            </Grid>
                            <Grid item className="padr0" sm={6} xs={12}>
                                
                                <div className="input-group">
                                    <label className="input-label" >DOB</label>
                                    <Controller as={TextField}  type="date" name="dob" defaultValue="" placeholder="yyyy-mm-dd" className="form-fields date-picker" control={control} />                            
                                </div>
                                <div className="input-group">
                                    <label className="input-label" >Contact Email ID</label>
                                    <Controller as={TextField}  type="text" disabled name="email" defaultValue="" placeholder="example@gmail.com" className="form-fields" control={control}  />                            
                                </div>
                                
                            </Grid>
                            
                        </Grid>
                    </DialogContent>
                    <DialogActions className="edit-member-profile-actions">
                        <Button  className="btn btn-secondary btn-delete display-none" color="primary">
                            DELETE
                        </Button>
                        <Button   onClick={handledeactiveClick} className="btn btn-secondary btn-delete" color="primary">
                            DEACTIVATE
                        </Button>
                        <Button onClick={handleCloseClick} className="btn btn-secondary" color="primary">
                            CANCEL
                        </Button>
                        <Button color="primary" className="btn btn-primary button" onClick={handleSubmit(onSubmit)}>
                            UPDATE   
                        </Button>
                    </DialogActions>
                </form>
        </Dialog>
    )
}
