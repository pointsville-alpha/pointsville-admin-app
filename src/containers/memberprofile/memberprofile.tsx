import React ,{useEffect,useState}from 'react';
import { useHistory,useLocation ,RouteComponentProps} from "react-router-dom";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import DoneIcon from '@material-ui/icons/Done';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import EditMemberProfile from './editmemberprofile';
import { useQuery, gql } from '@apollo/client';
import moment from 'moment';
import '../../constant.css';
import './memberprofile.css';
import ProfileImg from '../../assets/profile-img.png';
import { getDefaultValues } from '@apollo/client/utilities';

interface LocationState extends RouteComponentProps {
    state ?: string;
}

const GET_USER_BY_ID = gql`
  query getUsers($userId: String!) {
    getUserWithId(userId: $userId) {
        id
        username
        givenName
        email
        dob
        address
        phoneNumber
        isActive

    }
  }
`;

export default function MemberProfile() {
    const {state} = useLocation<LocationState>();
    const [userDetails,setUserDetails] = useState<any>({});
    const [userId,setUserId] =useState<any>();
    const { loading, data,refetch } = useQuery<any>(GET_USER_BY_ID, { variables: { userId: state } });
    const history = useHistory();
    const backClick = () => history.push('/managemembers');
    const [open, setOpen] = React.useState(false);
    useEffect(() => {
        if(!loading){
            console.log(data);
            setUserDetails(data.getUserWithId);   
        }
    },[data])
    
    const handleClickOpen = (userId:string) => {
        setOpen(true);
        setUserId(userId);
    };

    const handleClose = () => {
        setOpen(false);
    };
    
   
    useEffect(() => {
        console.log("useEffect");
        refetch();
    },[open]);

    return (
    <div className="detail-page member-profile">
        <div className="page-head p-relative padl-40">
            <Grid container sm={12}>
                <Grid item sm={6}>
                    <span className="back-icon" onClick={backClick}></span>
                    <h2 className="page-title">MEMBER PROFILE</h2>
                </Grid>
                <Grid className="align-right" item sm={6}>
                    <button onClick={()=>handleClickOpen(userDetails.username)} className="btn btn-secondary edit-btn"><span className="btn-icon"></span> <label className="btn-caption">Edit Profile</label></button>
                </Grid>
            </Grid>
        </div>
        <div className="page-body padl-30" >
            <Grid container className="rm-right" spacing={3} sm={12}>
                <Grid item sm={3} spacing={1}>
                    <Paper className="">
                        <div className="profile-details">
                            <img className="profile-img" src={ProfileImg}/>
                            <h5 className="profile-name">{userDetails.givenName}</h5>
                            <label className="label-txt">Walled ID :{userDetails.walletId}</label>
                            <label className="label-txt-small">Member since:{userDetails.userSince}</label>
                            <div className="chip-section">
                                <Chip className={userDetails.isActive?"status green":"status pending"} variant="outlined" label={userDetails.isActive?"Active":"InActive"} />
                                {/* <Chip className="status green" icon={<DoneIcon />} variant="outlined" label="Inactive" /> */}
                            </div>
                            <div className="revenue-section">
                                <div className="revenue-parent">
                                    <span className="data-bold" >3000</span>
                                    <label className="sub-caption">Redeemed</label>
                                </div>
                                <div className="revenue-parent">
                                    <span className="data-bold" >3000</span>
                                    <label className="sub-caption">Redeemed</label>
                                </div>
                            </div>
                        </div>
                    </Paper>
                </Grid>
                <Grid item sm={9} spacing={1}>
                    <Paper>
                        <div className="info-section personal-info-section">
                             <h5 className="section-heading">Personal Info</h5>
                             <Grid container sm={12} spacing={3}>
                                 <Grid container spacing={3} sm={6}>
                                     <Grid item sm={6}>
                                         <label className="label-txt">Full Name</label>
                                        <label className="field-txt">{userDetails.givenName}</label>
                                     </Grid>
                                     <Grid item sm={6}>
                                        <label className="label-txt">Phone Number</label>
                                        <label className="field-txt">{userDetails.phoneNumber}</label>
                                     </Grid>
                                     <Grid item sm={6}>
                                        <label className="label-txt">DOB</label>
                                        <label className="field-txt">{moment(userDetails.dob).format("DD/MM/YYYY")}</label>
                                     </Grid>
                                     <Grid item sm={6}>
                                        <label className="label-txt">Email ID</label>
                                        <label className="field-txt">{userDetails.email}</label>
                                     </Grid>
                                    </Grid>
                                    <Grid item sm={6}>
                                        <label className="label-txt">Address</label>
                                        <address className="field-txt address-field">
                                            {userDetails.address}
                                        </address>
                                 </Grid>
                             </Grid>
                        </div>
                    </Paper>
                    <Paper className="kyc-paper">
                        <div className="info-section kyc-info-section">
                             <h5 className="section-heading">Kyc Info</h5>
                             <div className="coming-soon-section">
                                <h5 className="page-title">Coming soon</h5>
                             </div>
                             <Grid className="display-none" container sm={12} spacing={3}>
                                     <Grid className="kyc-verification-container" container sm={7}>
                                         <Grid container sm={6}>
                                             <h5 className="section-title">Photo Id verification</h5>
                                            <Grid item sm={6}>
                                                <label className="label-txt">Full Name</label>
                                                <label className="field-txt">Katherine Walter</label>
                                            </Grid>
                                            <Grid item sm={6}>
                                                <label className="label-txt">Full Name</label>
                                                <label className="field-txt">Katherine Walter</label>
                                            </Grid>
                                         </Grid>
                                         <Grid item sm={6} className="align-right">
                                            <CheckCircleIcon/>
                                         </Grid>
                                     </Grid>
                                     <Grid className="kyc-verification-container" container sm={7}>
                                        <Grid container sm={6}>
                                            <h5 className="section-title">Photo Id verification</h5>
                                            <Grid item sm={6}>
                                                <label className="label-txt">Full Name</label>
                                                <label className="field-txt">Katherine Walter</label>
                                            </Grid>
                                            <Grid item sm={6}>
                                                <label className="label-txt">Full Name</label>
                                                <label className="field-txt">Katherine Walter</label>
                                            </Grid>
                                        </Grid>
                                        <Grid item sm={6} className="align-right">
                                            <CheckCircleIcon/>
                                         </Grid>
                                     </Grid>
                                     <Grid className="kyc-verification-container" container sm={7}>
                                        <Grid container sm={6}>
                                            <h5 className="section-title">Photo Id verification</h5>
                                            <Grid item sm={6}>
                                                <label className="label-txt">Full Name</label>
                                                <label className="field-txt">Katherine Walter</label>
                                            </Grid>
                                            <Grid item sm={6}>
                                                <label className="label-txt">Full Name</label>
                                                <label className="field-txt">Katherine Walter</label>
                                            </Grid>
                                        </Grid>
                                        <Grid item sm={6} className="align-right">
                                            <CheckCircleIcon/>
                                         </Grid>
                                     </Grid>               
                             </Grid>
                        </div>
                    </Paper>
                </Grid> 
            </Grid>
        </div>
        <EditMemberProfile onOpen={open} handleCloseClick={handleClose} memberId ={userId}/>
        
    </div>
    )
}
