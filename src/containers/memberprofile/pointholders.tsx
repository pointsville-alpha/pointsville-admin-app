import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import DropdownSelect from '../../components/Dropdown/dropdownselect';
import Button from "@material-ui/core/Button";
import '../../constant.css';
import '../transaction/transactions.css';

const useStyles = makeStyles({
  
});

function createData(memberName: string, emailId: string, phoneNumber:string, walletId:string,walletPoints: string, balance: string, memberSince: string,kycStatus:string,status:string) {
  return { memberName, emailId, phoneNumber,walletId, walletPoints, balance, memberSince ,kycStatus,status };
}

const rows = [
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', 'pending','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', 'pending','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', 'pending','Active'),
    createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active'),
   
  ];

export default function PointHolderList() {
    const classes = useStyles({
        container : {
            padding:0,
        }
    });
    return (
    <div className="list-page transaction-list-wrapper">
        <Grid container xs={12} sm={12}>
                <Grid  item sm={12} xs={12}>
                    <Paper className="table-wrapper">
                    <Grid container sm={12}>
                        <Grid item sm={8}>
                            <label className="table-heading">POINT HOLDER LIST</label>
                        </Grid>
                        <Grid  item sm={4} className="select-with-label">
                            <label>Sort By</label>
                            <DropdownSelect/>
                        </Grid>
                    </Grid>
                    <TableContainer className="table-container" style={{ height: 450, width: '100%' }} component={Paper}>
                        <Table stickyHeader className="" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Member Name</TableCell>
                                    <TableCell align="left">Email Id</TableCell>
                                    <TableCell align="left">Phone Number</TableCell>
                                    <TableCell align="left">Wallet ID</TableCell>
                                    <TableCell align="left">Wallet Points</TableCell>
                                    <TableCell align="left">$ Balance</TableCell>
                                    <TableCell align="left">Member Since</TableCell>
                                    <TableCell align="left">KYC</TableCell>
                                    <TableCell align="left">Status</TableCell>
                                </TableRow>
                            </TableHead> 
                            <TableBody>
                            {rows.map((row) => (
                                <TableRow >
                                <TableCell className="label-parent" padding="none" >
                                    <img className="profile-img " src="https://png.pngtree.com/png-vector/20190321/ourmid/pngtree-vector-users-icon-png-image_856952.jpg" />
                                    <label className="membername">{row.memberName}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{row.emailId}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{row.phoneNumber}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{row.walletId}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{row.walletPoints}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">${row.balance}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{row.memberSince}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                       {(row.kycStatus == "pending") ? <Chip className="status pending" label={row.kycStatus} variant="outlined" />:''}
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{row.status}</label>
                                </TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Paper>    
            </Grid>
        </Grid>
    </div>
    )
}
