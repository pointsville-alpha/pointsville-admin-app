import React, { useState } from "react";
import ReactDOM from "react-dom";
import MUIDataTable from "mui-datatables";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import TransactionLists from '../transaction/transactions';


export default function DataTable(){
const columns:any = [
    {name:"memberName",options:{ filter:false,customBodyRender:(value:any, tableMeta:any, updateValue:any) => {return (<div><img className='profile-img' src='https://png.pngtree.com/png-vector/20190321/ourmid/pngtree-vector-users-icon-png-image_856952.jpg' /><label className='membername'>Kathirine Walter</label></div>)},}},
     "emailId", "phoneNumber","walletId", "walletPoints", "balance", "memberSince" ,"kycStatus","status","empty"];
  
  const options:any = {
    filter: true,
    filterType: "dropdown",
    responsive:"standard",
    print:false,
    download : false,
    pagination : false,
    viewColumns : false,
    selectableRowsHideCheckboxes: true,
    fixedSelectColumn: false,
    selectToolbarPlacement: "none",
    selectableRowsHeader: false,
    selectableRows: "none",
    selectableRowsOnClick: false
  }


  function createData(memberName: any, emailId: string, phoneNumber:string, walletId:string,walletPoints: string, balance: string, memberSince: string,kycStatus:string,status:string,check:string) {
    return { memberName, emailId, phoneNumber,walletId, walletPoints, balance, memberSince ,kycStatus,status,check };
  }
  
  const rows = [
      createData({n:'Kathirine Walter',t:"d"}, 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', 'pending','Active','d'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','d'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','s'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','d'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','g'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', 'pending','Active','f'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','f'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','h'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', 'pending','Active','h'),
      createData('Kathirine Walter', 'kathirine@gmail.com', '+150393 9390', '9484 9768 0930 3939 30', '120','120','27 Aug 2020', '','Active','t'),
     
    ];
  return (
    <div>
      
      <MUIDataTable
        title={"ACME Employee list"}
        data={rows}
        columns={columns}
        options={options}
      />
    </div>
  );
}

