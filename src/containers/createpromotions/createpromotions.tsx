import React, { useState ,useEffect} from 'react'
import { useHistory,useParams } from "react-router-dom";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import Paper from '@material-ui/core/Paper';
import {
    Select,
    MenuItem,
    Switch,
  } from "@material-ui/core";
import Chip from '@material-ui/core/Chip';
import Grid from '@material-ui/core/Grid';
import '../../constant.css';
import '../manageclubs/manageclubs.css';
import './createpromotions.css';
import SponsorImg from '../../assets/toyota.png';
import PromotionMap from './promotionmap';
import { useQuery, gql, useMutation } from '@apollo/client';
import { stringify } from 'querystring';
interface Props {
    onOpen : boolean,
    handleCloseClick: () => void,
    promoId ?: any
}


interface MutationFun {
    createPromo : () => void;
}

interface ParamTypes {
    orgId: string;
}
interface IFormAddPromo { 

    promoCodePrefix ?: string | undefined;
    promoCodePoints  : string ;
    promoType :string | undefined;
    validity : string | undefined;
    validityDay : string;
    promoTotalCount: string ;
    sponsorId:string | undefined;
    pointsAvailable ?: number | undefined;
    noPointsPerPromo ?: number ;

    promoSentToMail?:string | null;
    promoTitle?: string | undefined;
    promoMessage?: string | undefined;
    promoSentTOClub?: string | undefined;
    notifyMessage ?: string | undefined;
    huntLocation ?: string | undefined;
}
let defaultValues = {
    promoCodePrefix : "PROMO",
    promoCodePoints : "",
    promoType: "",
    validity:"",
    validityDay:"",
    promoTotalCount : "0",
    sponsorId: "",
    pointsAvailable : 100,
    noPointsPerPromo:10,

    promoSentToMail:"",
    promoTitle :"Promo Title!",
    promoMessage:"Promo Message",
    promoSentTOClub : "",
    notifyMessage : "Promo Notification",
    huntLocation:""
  };

  export const GET_SPONSOR_QUERY = gql`
    query GetSponsorInventory($clubId : String!) {
        getSponsorWithOrgId (clubId : $clubId){
        id
        sponsorName
        sponsorLogo
    }
    }
    `;


const CREATE_PROMO = gql`
mutation createPromo($data : NewPromoInput!){
    createPromo(data:$data)
    {
      id
    }
  }
`
const UPDATE_PROMO = gql`
mutation updatePromos($id : String!,$data: UpdatePromoInput! ){
    updatePromo(id: $id, data: $data ){
        promoType
    }
   }`;
const PROMOTION_BY_ID = gql`
query getPromoFindOne($id: String!) {
    getPromoWithId(id: $id) {
        promoCodePrefix
        promoCodePoints
        promoType
        validity
        validityDay
        promoTotalCount
        sponsorId
        id
        promoTitle
        pointsAvailable
        noPointsPerPromo
        promoSentToMail
        promoMessage
        promoSentTOClub
  }
}`;


export default function CreatePromotions({handleCloseClick,onOpen,promoId}:Props) {
     const promotionId = promoId ? promoId : ""; 
     const open  = onOpen;
     let { orgId } = useParams<ParamTypes>();
    const fieldEnabled =promotionId ? true : false;
    const [openToast,setOpenToast] = React.useState<any>(false);
    const { loading:sponsorLoading, data : getSponsor,refetch:refetchSponsor } = useQuery<any>(GET_SPONSOR_QUERY, { variables: { clubId: orgId } });
    const { loading:promoLoading, data:PromoDetails,refetch:refetchPromoWithID } = useQuery<any>(PROMOTION_BY_ID, { variables: { id: promotionId } });
    
    const Alert = (props: AlertProps)  => {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
      }
    const [promotionType,setPromotionType] = React.useState<any>("");
    const [promotion, setPromotion] = React.useState<any>(defaultValues);
    const [noofFieldCaption, setNoofFieldCaption] = React.useState("Promocodes");
    const [validityDate , setValidityDate] = React.useState(moment().format("D MMM YYYY"));
    const {handleSubmit, register, reset, control, errors ,setValue,getValues} = useForm<IFormAddPromo>({defaultValues,shouldUnregister:false});
    const [promoInfo,setPromoInfo] = useState<any>({});
    const [sponsorLogo,setSponsorLogo] = useState<any>("");
    const  promoInputValues = (promoInputValues:any) => {
            
            setValue("promoCodePrefix",promoInputValues.promoCodePrefix);
            setValue("promoCodePoints",promoInputValues.promoCodePoints);
            setValue("promoType",promoInputValues.promoType);
            setValue("validity",promoInputValues.validity);
            setValue("validityDay",promoInputValues.validityDay);
            setValue("promoTotalCount",promoInputValues.promoTotalCount);
            setValue("sponsorId",promoInputValues.sponsorId);
            setValue("pointsAvailable",promoInputValues.pointsAvailable);
            setValue("noPointsPerPromo",promoInputValues.noPointsPerPromo);
            setValue("promoSentToMail",promoInputValues.promoSentToMail);
            setValue("promoTitle",promoInputValues.promoTitle);
            setValue("promoMessage",promoInputValues.promoMessage);
            setValue("promoSentTOClub",promoInputValues.promoSentTOClub);
            setValue("notifyMessage",promoInputValues.notifyMessage);
            setValue("huntLocation",promoInputValues.huntLocation);
            setPromotionType(defaultValues.promoType); 
            setValidityDate(defaultValues.validity);    
    }
    
    useEffect(() => {
        refetchPromoWithID();
        if(!promoLoading && promotionId){
            defaultValues = {...PromoDetails.getPromoWithId};
            setPromotion(PromoDetails.getPromoWithId);
            promoInputValues(defaultValues);
        }else{
            defaultValues = {
                promoCodePrefix : "PROMO",
                promoCodePoints : "",
                promoType: "",
                validity:"",
                validityDay:"",
                promoTotalCount : "0",
                sponsorId: "",
                pointsAvailable : 100,
                noPointsPerPromo:10,
            
                promoSentToMail:"",
                promoTitle :"Promo Title!",
                promoMessage:"Promo Message",
                promoSentTOClub : "",
                notifyMessage : "Promo Notification",
                huntLocation:""
            };
            promoInputValues(defaultValues);
        }
    },[open,PromoDetails,setValue]);
   

    const [createPromo] = useMutation(CREATE_PROMO,{
        // after updating the post, we go to the home page
        onCompleted: () => handleCloseClick(),
        onError: (data) => {alert(data.message)}
      });
    const [updatePromo] = useMutation(UPDATE_PROMO,{
        // after updating the post, we go to the home page
        onCompleted: () => handleCloseClick(),
      });
    const onSubmit: SubmitHandler<IFormAddPromo> = (data:any) => {
        console.log(stringify(data));
        if(promoId){
            updatePromo({
                variables : 
                { id : promoId,
                    data : 
                    { 
                        promoCodePrefix : data.promoCodePrefix, 
                        promoCodePoints : parseInt(data.promoCodePoints),
                        promoType: data.promoType,
                        validity:validityDate,
                        validityDay:parseInt(data.validityDay),
                        promoTotalCount : parseInt(data.promoTotalCount),
                        sponsorId: data.sponsorId,
                        orgId : orgId,
                        pointsAvailable : parseInt(data.promoTotalCount),
                        noPointsPerPromo :parseInt(data.promoTotalCount),

                        promoSentToMail:data.promoSentToMail?data.promoSentToMail:"" ,
                        promoTitle:data.promoTitle?data.promoTitle:"",
                        promoMessage : data.promoMessage?data.promoMessage:"",
                        promoSentTOClub : data.promoSentTOClub ? data.promoSentTOClub : "",
                        // notifyMessage : data.notifyMessage,
                        // huntLocation : data.huntLocation
                    }
                }
            })
        }else{
            createPromo({
                variables : 
                { data : 
                    { 
                        promoCodePrefix : data.promoCodePrefix, 
                        promoCodePoints : parseInt(data.promoCodePoints),
                        promoType: data.promoType,
                        validity:validityDate,
                        validityDay:parseInt(data.validityDay),

                        promoTotalCount : parseInt(data.promoTotalCount),
                        sponsorId: data.sponsorId,
                        sponsorLogo : sponsorLogo,
                        orgId : orgId,
                        pointsAvailable : parseInt(data.promoTotalCount),
                        noPointsPerPromo : parseInt(data.promoTotalCount),

                        promoSentToMail:data.promoSentToMail?data.promoSentToMail:"",
                        promoTitle : data.promoTitle?data.promoTitle:"",
                        promoMessage : data.promoMessage?data.promoMessage:"",
                        promoSentTOClub : data.promoSentTOClub ? data.promoSentTOClub : "",
                        // notifyMessage : data.notifyMessage,
                        // huntLocation : data.huntLocation
                    }
                }
            })
        }
    }


    const handleChange = (e: React.ChangeEvent<{ value: string | unknown }>) => {
         const targetType = (e.target as HTMLInputElement).value;
         const targetName = (e.target as HTMLInputElement).name;
         setValue(targetName ,targetType);
         setPromotionType(targetType);
         setNoofFieldCaption("Promocodes");
         if(targetType === "Hunt"){
              setNoofFieldCaption("Coins");
         }

    }; 
    const handleChangeField = (e: React.ChangeEvent<any>) => {
        const targetValue = (e.target as HTMLInputElement).value;
        const targetName = (e.target as HTMLInputElement).name;
        setValue(targetName ,targetValue);
        if(targetName == "sponsorId"){
            let selectedSponsor = getSponsor.getSponsorWithOrgId.find((i:any) => i.id === targetValue);
            setSponsorLogo(selectedSponsor.sponsorLogo);
        }
        if( targetName == "validityDay"){
             //console.log(targetValue);
             setValidityDate(moment().add(targetValue,'days').format("D MMM YYYY"));
        }
        setPromotion({ ...promotion, [targetName]: targetValue });
    };
    
    return (
        <Dialog open={onOpen} onClose={handleCloseClick} aria-labelledby="form-dialog-title" className="common-dialog-container org-container">
                <form className="form-container" onSubmit={handleSubmit(onSubmit)}>
                    <DialogContent className="pad0">
                        <Grid className="form-grid" container sm={12} spacing={2}>
                            <Grid item sm={12}>
                                <h5 id="form-dialog-title" className="section-heading">Create Promotions</h5>
                            </Grid>
                            <Grid container className="borderR padd-20" sm={6} xs={12} spacing={3}>
                                <Grid container sm={12} spacing={3}>
                                <Grid item sm={6}>
                                    <div className="input-group">
                                        <label className="input-label">Promotion Type</label>
                                        
                                        <Controller
                                         name="promoType"
                                         control={control} 
                                         rules={{ required: true}}
                                            render = {({value}) => (
                                            <Select displayEmpty name="promoType" value={value} disabled={fieldEnabled} className={`custom-select form-select  ${errors.promoType && 'error'}`} onChange={(e) => handleChange(e)}
                                              >
                                                <MenuItem value="">Select</MenuItem>
                                                <MenuItem value={"Email"}>Email</MenuItem>
                                                <MenuItem value={"Notification"}>App Notification</MenuItem>
                                                <MenuItem value={"Hunt"}>Hunt</MenuItem>
                                            </Select>
                                          )
                                        }  
                                        />
                                    {errors.promoType && errors.promoType.type == "required" && (<label className="error-message">Field is requried</label>)} 
                                    </div>
                                </Grid>
                                <Grid item sm={6}>
                                    <div className="input-group">
                                        <label className="input-label">Sponsor</label>
                                        <Controller
                                            name="sponsorId"
                                            rules={{ required: true}}
                                            control={control}
                                            render = {({value}) => (
                                                <Select displayEmpty name="sponsorId" disabled={fieldEnabled}  className={`custom-select form-select  ${errors.sponsorId && 'error'}`} value={value}     onChange={(e) => handleChangeField(e)}
                                                  >
                                                    <MenuItem value="">Select</MenuItem>
                                                    {sponsorLoading ?(
                                                        <MenuItem value={"Loading"}>Loading ...</MenuItem>
                                                    ) : ( 
                                                    getSponsor.getSponsorWithOrgId.map((sponsor : any) => (
                                                        <MenuItem key={sponsor.id} data-img={sponsor.sponsorLogo} value={sponsor.id}>{sponsor.sponsorName}</MenuItem>
                                                    ))
                                                    )}
                                                </Select>
                                              )
                                            }  
                                            
                                        />
                                    {errors.sponsorId && errors.sponsorId.type == "required" && (<label className="error-message">Field is requried</label>)} 
                                    </div>
                                </Grid>
                                <Grid item sm={6}>
                                    <div className="input-group">
                                        <label className="input-label">Code Prefix</label>
                                        <Controller  control={control}  name="promoCodePrefix" rules={{ required: true, validate : (value) =>value.length<=6}}  render = { ({value}) => ( <TextField name="promoCodePrefix" disabled={fieldEnabled} value={value} placeholder="Code Prefix" onChange={(e) => handleChangeField(e)} className={`form-fields ${errors.promoCodePrefix && 'error'}`}  />)}  />                            
                                        {errors.promoCodePrefix && errors.promoCodePrefix.type == "validate" && (<label className="error-message">Code should be Max 4 Characters</label>)}  
                                        {errors.promoCodePrefix && errors.promoCodePrefix.type == "required" &&  <label className="error-message">Field is required</label>}
                                    </div>
                                </Grid>
                                <Grid item sm={6}>
                                    <div className="input-group">
                                        <label className="input-label" >Points</label>
                                        <Controller
                                            name="promoCodePoints"
                                            control={control} 
                                            rules={{ required: true}}
        
                                            render = {({value}) => (
                                                <Select displayEmpty name="promoCodePoints"  value={value} disabled={fieldEnabled} className={`custom-select form-select  ${errors.promoCodePoints && 'error'}`}   onChange={(e) => handleChangeField(e)}
                                                  >
                                                    <MenuItem value="">Select</MenuItem>
                                                    <MenuItem value={"100"}>100</MenuItem>
                                                    <MenuItem value={"200"}>200</MenuItem>
                                                    <MenuItem value={"300"}>300</MenuItem>
                                                    <MenuItem value={"400"}>400</MenuItem>
                                                    <MenuItem value={"500"}>500</MenuItem>
                                                    <MenuItem value={"600"}>600</MenuItem>
                                                    <MenuItem value={"700"}>700</MenuItem>
                                                    <MenuItem value={"800"}>800</MenuItem>
                                                    <MenuItem value={"900"}>900</MenuItem>
                                                    <MenuItem value={"1000"}>1000</MenuItem>

                                                </Select>
                                              )
                                            }  
                                        />
                                        {errors.promoCodePoints && errors.promoCodePoints.type == "required" && (<label className="error-message">Field is requried</label>)} 
                                    </div>
                                </Grid>
                                <Grid item sm={6}>
                                    <div className="input-group">
                                        <label className="input-label" >No.of {noofFieldCaption}</label>
                                        <Controller control={control}  rules={{required: true}} name="promoTotalCount"  render={({value}) => (<TextField type="number"  disabled={fieldEnabled} value={value} name="promoTotalCount" placeholder={"No of "+noofFieldCaption} onChange={(e) => handleChangeField(e)} className={`form-fields ${errors.promoTotalCount && 'error'}`} />)}  />                            
                                        {errors.promoTotalCount && errors.promoTotalCount.type == "required" && (<label className="error-message">Field is requried</label>)} 
                                    </div>
                                </Grid>
                                <Grid item sm={6}>
                                    <div className="input-group">
                                        <label className="input-label">Validity</label>
                                        <Controller
                                            name="validityDay"
                                            control={control}
                                            rules={{ required: true}}
                                            render = {({value}) => (
                                                <Select displayEmpty name="validityDay" disabled={fieldEnabled} value={value} className={`custom-select form-select  ${errors.validityDay && 'error'}`}  onChange={(e) => handleChangeField(e)}
                                                  >
                                                    <MenuItem value={""}>Select</MenuItem>
                                                    <MenuItem value={"1"}>1 Day</MenuItem>
                                                    <MenuItem value={"2"}>2 Days</MenuItem>
                                                    <MenuItem value={"3"}>3 Days</MenuItem>
                                                    <MenuItem value={"4"}>4 Days</MenuItem>
                                                    <MenuItem value={"5"}>5 Days</MenuItem>
                                                    <MenuItem value={"6"}>6 Days</MenuItem>
                                                    <MenuItem value={"7"}>7 Days</MenuItem>
                                                </Select>
                                              )
                                            }
                                        />
                                        {errors.validityDay && errors.validityDay.type == "required" && (<label className="error-message">Field is requried</label>)} 
                                    </div>
                                </Grid> 
                                </Grid>
                                <Grid container className="promo-dynfield-container" sm={12} xs={12}>
                                    <Grid item sm={12}>
                                        {promotionType == "Email" && 
                                        
                                        <div className="info-section email-info">
                                            <label className="section-heading sub-heading">
                                                Email info
                                            </label>
                                            <Grid item sm={12}>
                                                <div className="input-group">
                                                    <label className="input-label" >Email</label>
                                                    <Controller control={control} name="promoSentToMail" render={({value}) => (<TextField  type="text" value={value} disabled={fieldEnabled} name="promoSentToMail" placeholder="example@gmail.com" className="form-fields"   onChange={(e) => handleChangeField(e)}/>)}  />                            
                                                </div>
                                            </Grid>
                                            <Grid item sm={12}>
                                                <div className="input-group">
                                                    <label className="input-label" >Title</label>
                                                    <Controller control={control}  name="promoTitle" render={({value}) => (<TextField type="text"  disabled={fieldEnabled}value={value} name="promoTitle" placeholder="promoTitle" className="form-fields"  onChange={(e) => handleChangeField(e)}/>)}  />                            
                                                </div>
                                            </Grid>
                                            <Grid item sm={12}>
                                                <div className="input-group textarea-group">
                                                    <label className="input-label">Message</label>
                                                    <Controller control={control} name="promoMessage" render={({value}) => (<TextareaAutosize value={value} disabled={fieldEnabled} aria-label="minimum height" rowsMin={3} placeholder="Address"   name="promoMessage"  className="form-fields textarea-field MuiInputBase-input MuiInput-input" onChange={(e) => handleChangeField(e)}  />)}  />                            
                                                </div>
                                            </Grid>
                                        </div>
                                        } 
                                        { promotionType == "Notification" && 
                                        <div className="info-section notification-info">
                                            <label className="section-heading sub-heading">
                                                Notification info
                                            </label>
                                            <Grid item sm={12}>
                                                <div className="input-group">
                                                    <label className="input-label" >Notify to</label>
                                                    <Controller
                                                        name="promoSentTOClub"
                                                        control={control}
                                                        render = {({value}) => (
                                                            <Select displayEmpty disabled={fieldEnabled} name="promoSentTOClub" value={value}  className={`custom-select form-select  ${errors.validity && 'error'}`}  onChange={(e) => handleChangeField(e)}
                                                              >
                                                                <MenuItem value="">Select</MenuItem>
                                                                <MenuItem value={"all"}>All Org</MenuItem>
                                                             
                                                            </Select>
                                                          )
                                                        }
                                                      
                                                    />
                                                </div>
                                            </Grid>
                                            <Grid item sm={12}>
                                                <div className="input-group">
                                                    <label className="input-label" >Message</label>
                                                    <Controller control={control}  name="promoMessage" render={({value}) => (<TextField type="text" disabled={fieldEnabled} name="promoMessage" value={value} placeholder="Message" className="form-fields"   onChange={(e) => handleChangeField(e)}/>)}  />                            
                                                </div>
                                            </Grid>
                                        </div>
                                        }
                                        {promotionType == "Hunt" && 
                                        <div className="info-section hunt-info">
                                            <label className="section-heading sub-heading">
                                                Hunt info
                                            </label>
                                            <Grid item sm={12}>
                                                <div className="input-group location-field">
                                                    <label className="input-label" >Location</label>
                                                    <Controller control={control} name="huntLocation" render={({value}) => (<TextField   disabled={fieldEnabled} type="text" name="huntlocation" value={value} placeholder="Select location" className="form-fields"  />)}/>                            
                                                </div>
                                            </Grid>
                                            <Grid item sm={12}>
                                                <div className="input-group">
                                                    <label className="input-label" >Points Distribution</label>
                                                    <ul className="point-distribution">
                                                        <li className="selected">10</li>
                                                        <li>20</li>
                                                        <li>50</li>
                                                        <li>100</li>
                                                    </ul>
                                                </div>
                                            </Grid>
                                        </div>
                                        } 
                                    </Grid>
                                </Grid>  
                            </Grid>

                            <Grid item sm={6} xs={12}>
                                     <label className="section-heading sub-heading mrgb-20">
                                    Promotion Preview</label>
                                    { promotionType == "Email" && 
                                    <div className="email-preview">
                                        <Grid container sm={12}>
                                            <div className="preview-wrapper">
                                                <div className="preview-head">
                                                    <Grid container className="flx-align-center" sm={12}>
                                                        <Grid item sm={3}>
                                                            <img src={sponsorLogo || "https://us.v-cdn.net/6022045/uploads/defaultavatar.png"} alt="sponsorlogo" className="sponsor-logo"/>
                                                        </Grid>
                                                        <Grid item sm={9}>
                                                            <label className="section-heading sub-heading email-title">{promotion.promoTitle}</label>
                                                        </Grid>
                                                    </Grid>
                                                </div>
                                                <div className="preview-body">
                                                    <Grid container sm={12}>
                                                        <Grid item sm={12}>
                                                            <p className="message-preview sub-caption">{promotion.promoMessage}</p>
                                                        </Grid>
                                                        <Grid className="promo-code-sec" item sm={12}>
                                                            <label className="promocode-preview">{promotion.promoCodePrefix + promotion.promoCodePoints}</label>
                                                            <label className="update-date-txt">Valid till {validityDate}</label>
                                                        </Grid>
                                                    </Grid>
                                                </div>
                                            </div>
                                        </Grid>
                                    </div>
                                    }
                                    {promotionType == "Notification" &&   
                                    <div className="appnotification-preview p-relative ">
                                        <Grid container sm={12}>
                                            <div className="preview-wrapper">
                                                <div className="preview-body">
                                                    <Grid container className="flx-align-center" sm={12} spacing={2}>
                                                            <img src={sponsorLogo || "https://us.v-cdn.net/6022045/uploads/defaultavatar.png"} alt="sponsorlogo" className="sponsor-logo"/>
                                                            <div className="title-container">
                                                            <label className="section-heading sub-heading notify-title">{promotion.promoTitle}</label>
                                                            <label className="update-date-txt">Valid till {validityDate}</label>
                                                            </div>
                                                             <label className="promocode">{promotion.promoCodePrefix + promotion.promoCodePoints}</label>
                                                       
                                                    </Grid>
                                                </div>
                                            </div>
                                        </Grid>
                                    </div>
                                    } 
                                    {promotionType == "Hunt" &&   
                                    <div className="map-preview">
                                        <Grid container sm={12}>
                                            <PromotionMap/>
                                        </Grid>
                                    </div>
                                    }
                            </Grid>
                        </Grid> 
                    </DialogContent>
                    <DialogActions className="edit-member-profile-actions" style={{display:(fieldEnabled)?"none":"flex"}}>
                        <Button onClick={handleCloseClick} className="btn btn-secondary" color="primary">
                            CANCEL
                        </Button>
                        <Button color="primary" className="btn btn-primary button" onClick={handleSubmit(onSubmit)}>
                            SEND PROMOTION   
                        </Button>
                    </DialogActions>
                </form>
                
        </Dialog>
    )
}
