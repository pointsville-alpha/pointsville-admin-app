import React, { useState } from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from './marker';



export default function  PromotionMap() {
  const getMapOptions = (maps: any) => {
    return {
      disableDefaultUI: true,
      mapTypeControl: true,
      streetViewControl: true,
      zoomControl :true,
      styles: [{ featureType: 'poi', elementType: 'labels', stylers: [{ visibility: 'on' }] }],
    };
  };

    const [center, setCenter] = useState({lat: 11.0168, lng: 76.9558 });
    const [zoom, setZoom] = useState(11);
    return (
        <div style={{ height: '70vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyALqFP-Byo_LPoJlviyTk2OmITJxKRFMM4' }}
          defaultCenter={center}
          defaultZoom={zoom}
          options={getMapOptions}
         
        >
          <Marker
            lat={11.0168}
            lng={76.9558}
            name="My Marker"
            color="blue"
          />
        </GoogleMapReact>
      </div>
    );
}


