import React,{useEffect,useState} from 'react';
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import { useQuery, gql } from '@apollo/client';
import Grid from '@material-ui/core/Grid';
import DropdownSelect from '../../components/Dropdown/dropdownselect';
import Button from "@material-ui/core/Button";
import moment from 'moment';
import '../../constant.css';
import './manageuser.css';


interface MemberInventory {
    id: string;
    promoType: string;
}

interface MemberInventoryData {
    memberInventory: MemberInventory[];
}

interface MemberInventoryVars {
    year: number;
}

  export const GET_MEMBER_QUERY = gql`
    query GetUserData {
        getUser{
            id
            givenName
            username
            email
            phoneNumber
            isActive
            walletId
            walletBalance
            userPrice
            createdAt
        }
    }
`;

export default function ManageMembers() {
   
    const history = useHistory();
    const {data, loading, refetch} = useQuery<any>(GET_MEMBER_QUERY);
    const [total,setTotal] = useState(0);
    const [active,setActive] = useState(0);
    const [inActive,setInActive] =  useState(0);

    const memberprofileClick = (userId:string) => {
        history.push({pathname:'/memberprofile',state:userId});
    }

    useEffect(() => {   
        refetch();
        if(!loading){   
            console.log(data.getUser);
            const activeUser = data.getUser.filter( (user:any) => user.isActive === true);
            setActive(activeUser.length);
            const inActiveUser = data.getUser.filter( (user:any) => user.isActive === false);
            setInActive(inActiveUser.length);
            setTotal(data.getUser.length);
        }
    },[data]);

    return (
    <div className="list-page manage-member-wrapper">
        <h2 className="page-title">MANAGE MEMBERS</h2>
        <Grid container xs={12} sm={12}>
            <div>
                <Grid container className="item-status manage-member-counts" xs={12}>
                        <Grid item sm={2} xs={2}>
                            <span className="data-bold total-member">{total}</span><label className="sub-caption">Total Member</label>
                        </Grid>
                        <Grid item sm={2} xs={2}>
                            <span className="data-bold active-member">{active}</span><label className="sub-caption">Active Member</label>
                        </Grid>
                        <Grid item sm={2} xs={2}>
                            <span className="data-bold inactive-member">{inActive}</span><label className="sub-caption">Inactive Member</label>
                        </Grid>
                        {/* <Grid item sm={1} xs={2}>
                            <span className="data-bold kyc-pending" >3000</span><label className="sub-caption">KYC Pending</label>
                        </Grid> */}
                        {/* <Grid item sm={1} xs={2}>
                            <span className="data-bold kyc-rejected">3000</span><label className="sub-caption">KYC Rejected</label>
                        </Grid> */}
                    </Grid>
            </div>
                <Grid  item sm={12} xs={12}>
                    <Paper className="table-wrapper">
                    <Grid container sm={12} className="mrgb-20">
                        <Grid item sm={8}>
                            <label className="table-heading">MEMBER LIST</label>
                        </Grid>
                        <Grid  item sm={4} className="select-with-label">
                            <label>Sort By</label>
                            <DropdownSelect/>
                        </Grid>
                    </Grid>
                    <TableContainer className="table-container" style={{ height: 450, width: '100%' }} component={Paper}>
                        <Table stickyHeader className="" aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Member Name</TableCell>
                                    <TableCell align="left">Email Id</TableCell>
                                    <TableCell align="left">Phone Number</TableCell>
                                    <TableCell align="left">Wallet ID</TableCell>
                                    <TableCell align="left">Wallet Points</TableCell>
                                    <TableCell align="left">$ Balance</TableCell>
                                    <TableCell align="left">Member Since</TableCell>
                                    {/* <TableCell align="left">KYC</TableCell> */}
                                    <TableCell align="left">Status</TableCell>
                                </TableRow>
                            </TableHead> 
                            { (loading) ? ( <p>...Loading</p>) : (
                            <TableBody>
                            {data.getUser.map((user:any) => (
                                <TableRow className={user.isActive?"enable-row":"disabled-row"} onClick={() => {user.isActive && memberprofileClick(user.username)}} >
                                <TableCell className="label-parent" padding="none" >
                                    <img className="profile-img " src={user.profileImage || "https://png.pngtree.com/png-vector/20190321/ourmid/pngtree-vector-users-icon-png-image_856952.jpg"} />
                                    <label className="membername">{user.givenName}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{user.email}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                            <label className="">{user.phoneNumber}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                            <label className="">{user.walletId}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{user.userPrice}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                            <label className="">${user.walletBalance}</label>
                                </TableCell>
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{moment(parseInt(user.createdAt)).format('D MMM YYYY HH:MM')}</label>
                                </TableCell>
                                {/* <TableCell className="label-parent" padding="none" >
                                       {(row.kycStatus == "pending") ? <Chip className="status pending" label={row.kycStatus} variant="outlined" />:''}
                                </TableCell> */}
                                <TableCell className="label-parent" padding="none" >
                                    <label className="">{user.isActive? "Active":"InActive"}</label>
                                </TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                            )}
                        </Table>
                    </TableContainer>
                </Paper>    
            </Grid>
        </Grid>
    </div>
    )
}
