/* eslint-disable no-use-before-define */
import React from 'react';
import './loader.css';
interface Props {
  size ?: string,
}


export default function Loader({size}:Props) {
  
  return (
    <div className="sk-chase" style={{width:size,height:size}}>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
      <div className="sk-chase-dot"></div>
  </div>
  );
}

