/* eslint-disable no-use-before-define */
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import './dropdownselect.css';
import {
  Checkbox,
  Select,
  MenuItem,
  Switch,
  RadioGroup,
  FormControlLabel,
  ThemeProvider,
  Radio,
  createMuiTheme,
  Slider
} from "@material-ui/core";

export default function DropdownSelect() {
  const [age, setAge] = React.useState('0');

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setAge(event.target.value as string);
  };
  return (
      <div className="custom-select sortby">
        <Select onChange={handleChange} value={age}>
            <MenuItem value={0}>Recent</MenuItem>
            <MenuItem value={1}>A-Z</MenuItem>
            <MenuItem value={2}>Z-A</MenuItem>
          </Select>
      </div>
  );
}

